#!/bin/bash
for file in *.txt
do
        gsed -i "s/é/e/g" $file
	gsed -i "s/É/E/g" $file
	gsed -i "s/È/E/g" $file
	gsed -i "s/è/e/g" $file
	gsed -i "s/à/a/g" $file
	gsed -i "s/À/A/g" $file
	gsed -i "s/é/e/g" $file
	gsed -i "s/ù/u/g" $file
	gsed -i "s/ï/i/g" $file
	gsed -i "s/://g" $file
	gsed -i "s/\"//g" $file
	gsed -i "s/â/a/g" $file
done