<?xml version="1.0" encoding="UTF-8"?><schema xmlns="http://purl.oclc.org/dsdl/schematron" xmlns:cda="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" defaultPhase="CS8-20130326" queryBinding="xslt2" schemaVersion="CI-SIS_CertificatSanteCS8.sch">
    <title>Conformité d'un document CDAr2 au volet Certificat de santé de l'enfant (modèle CS8) du CI-SIS</title>
    <ns prefix="cda" uri="urn:hl7-org:v3"/>
    <ns prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
    <ns prefix="jdv" uri="http://esante.gouv.fr"/>
    <ns prefix="svs" uri="urn:ihe:iti:svs:2008"/>

                                    
    
    <!--Suppressed abstract pattern dansJeuDeValeurs was here-->
    <!--Suppressed abstract pattern IVL_TS was here-->   
    <!--Suppressed abstract pattern personName was here-->

                                          

    <pattern id="addr">
    <rule context="cda:addr">
        <let name="nba" value="count(@*)"/>
        <let name="nbch" value="count(*)"/>
        <let name="val" value="@*"/>
        <assert test="(             ($nba = 0 and $nbch &gt; 0) or             ($nba and name(@*) = 'use' and $nbch &gt; 0) or              ($nba = 1 and name(@*) = 'nullFlavor' and $nbch = 0 and             ($val = 'UNK' or $val = 'NASK' or $val = 'ASKU' or $val = 'NAV' or $val = 'MSK'))              )">
            Erreur de conformité CI-SIS : <name/> ne contient pas un attribut autorisé pour une adresse, 
            ou est vide et sans nullFlavor, ou contient une valeur de nullFlavor non admise.
        </assert>
        <assert test="$nbch = 0 or                         (cda:streetAddressLine and not(cda:postalCode) and not(cda:city) and not(cda:country) and not(cda:state)                         and not(cda:houseNumber) and not(cda:streetName)and not(cda:additionalLocator) and not(cda:unitID)                         and not(cda:postBox) and not(cda:precinct)) or                         (not(cda:streetAddressLine))                         ">
            Erreur de conformité CI-SIS : <name/> doit être structuré : 
            - soit sous la formes de lignes d'adresse (streetAddressLine)
            - soit sous la forme de composants élémentaires d'adresse
        </assert>
    </rule>
</pattern>    
    <pattern id="administrativeGenderCode">
    <p>Conformité du code sexe du patient ou du subject, nullFlavor autorisé</p>
    <rule context="cda:administrativeGenderCode">
        <let name="NF" value="@nullFlavor"/>
        <let name="sex" value="@code"/>
        <assert test="$sex = 'M' or $sex = 'F' or $sex = 'U' or $NF = 'UNK' or $NF = 'NASK' or $NF = 'ASKU' or $NF = 'NAV' or $NF = 'MSK'">
            Erreur de conformité CI-SIS : l'élément administrativeGenderCode doit être présent, avec code sexe ou un nullFlavor autorisé 
            (valeur trouvée <value-of select="@*"/>).
        </assert>
    </rule>
</pattern>
    <pattern id="assignedAuthor">
    <rule context="cda:assignedAuthor">
        <assert test="cda:addr"> Erreur de conformité CI-SIS : L'élément assignedAuthor doit
            comporter une adresse géographique (nullFlavor autorisé). </assert>
        <assert test="cda:telecom"> Erreur de conformité CI-SIS : L'élément assignedAuthor doit
            comporter une adresse telecom (nullFlavor autorisé). </assert>
    </rule>
</pattern>
    <pattern id="assignedEntity">
    <rule context="cda:assignedEntity">
        <assert test="./cda:id"> Erreur de conformité CI-SIS : L'élément "id" doit être présent sous
            assignedEntity. </assert>
        <assert test="cda:assignedPerson"> Erreur de conformité CI-SIS : L'élément
            "assignedPerson" doit être présent sous assignedEntity (nullFlavor autorisé). </assert>
        <assert test="cda:assignedPerson/cda:name or cda:assignedPerson/@nullFlavor"> 
            Erreur de conformité CI-SIS : 
            Si l'élément assignedPerson n'est pas vide avec un nullFlavor, alors il 
            doit comporter un élément fils "name" (nullFlavor autorisé). </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract personName--><pattern id="authenticatorName">
    <rule context="cda:authenticator/cda:assignedEntity/cda:assignedPerson/cda:name">
        <assert test="(                         (name(@*) = 'nullFlavor' and 1 and                            (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or                         ((./cda:family) and                        ((./cda:family[@qualifier='BR' or @qualifier='SP' or @qualifier='CL']) or not(./cda:family[@qualifier])))                     )">
            Erreur de conformité CI-SIS : L'élément <value-of select="'authenticator/assignedEntity/assignedPerson/name'"/>/family doit être présent 
            avec un attribut qualifier valorisé dans : BR (nom de famille), SP (nom d'usage) ou CL (pseudonyme)
            ou sans attribut qualifier. Valeur trouvée pour family : <value-of select="./cda:family"/>. Valeur trouvée pour family@qualifier : <value-of select="./cda:family/@qualifier"/>
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract personName--><pattern id="authorPersonName">
    <rule context="cda:assignedAuthor/cda:assignedPerson/cda:name">
        <assert test="(                         (name(@*) = 'nullFlavor' and 1 and                            (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or                         ((./cda:family) and                        ((./cda:family[@qualifier='BR' or @qualifier='SP' or @qualifier='CL']) or not(./cda:family[@qualifier])))                     )">
            Erreur de conformité CI-SIS : L'élément <value-of select="'assignedAuthor/assignedPerson/name'"/>/family doit être présent 
            avec un attribut qualifier valorisé dans : BR (nom de famille), SP (nom d'usage) ou CL (pseudonyme)
            ou sans attribut qualifier. Valeur trouvée pour family : <value-of select="./cda:family"/>. Valeur trouvée pour family@qualifier : <value-of select="./cda:family/@qualifier"/>
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="authorSpecialty">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="cda:ClinicalDocument/cda:author/cda:assignedAuthor/cda:code">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 1)">
           [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/author/assignedAuthor/code'"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (1 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/author/assignedAuthor/code'"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_authorSpecialty)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="'ClinicalDocument/author/assignedAuthor/code'"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_authorSpecialty"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract IVL_TS--><pattern id="authorTime">
    <p>
        Vérification de la conformité au CI-SIS d'un élément de type IVL_TS ou TS du standard CDAr2 :
        L'élément doit porter soit un attribut "value" soit un intervalle éventuellement semi-borné de sous-éléments "low", "high". 
        Alternativement, si l'attribut nullFlavor est autorisé, il doit porter l'une des valeurs admises par le CI-SIS. 
    </p>
    <rule context="cda:author/cda:time">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'author/time'"/>/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
        <assert test="(@* and not(*)) or (not(@*) and *)">
            Erreur de conformité CI-SIS : <value-of select="'author/time'"/> doit contenir soit un attribut soit des éléments fils.
        </assert>
        <assert test="(             (name(@*) = 'nullFlavor' and 1 and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             (name(@*) != 'nullFlavor')              )">
            Erreur de conformité CI-SIS : <value-of select="'author/time'"/> contient un 'nullFlavor' non autorisé ou porteur d'une valeur non admise.
        </assert>  
    </rule> 

    <rule context="cda:author/cda:time/cda:low">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'author/time'"/>/low/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
    </rule>
    
    <rule context="cda:author/cda:time/cda:high">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'author/time'"/>/high/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
     </rule>       

</pattern>
    <pattern id="representedCustodianOrganization">
    <rule context="cda:representedCustodianOrganization">
        <assert test="cda:addr"> Erreur de conformité CI-SIS : L'élément representedCustodianOrganization doit
            comporter une adresse géographique (nullFlavor autorisé). </assert>
        <assert test="cda:telecom"> Erreur de conformité CI-SIS : L'élément representedCustodianOrganization doit
            comporter une adresse telecom (nullFlavor autorisé). </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="documentCode">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="cda:ClinicalDocument/cda:code">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/code'"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/code'"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_typeCode)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="'ClinicalDocument/code'"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_typeCode"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract IVL_TS--><pattern id="documentEffectiveTime">
    <p>
        Vérification de la conformité au CI-SIS d'un élément de type IVL_TS ou TS du standard CDAr2 :
        L'élément doit porter soit un attribut "value" soit un intervalle éventuellement semi-borné de sous-éléments "low", "high". 
        Alternativement, si l'attribut nullFlavor est autorisé, il doit porter l'une des valeurs admises par le CI-SIS. 
    </p>
    <rule context="cda:ClinicalDocument/cda:effectiveTime">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/effectiveTime'"/>/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
        <assert test="(@* and not(*)) or (not(@*) and *)">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/effectiveTime'"/> doit contenir soit un attribut soit des éléments fils.
        </assert>
        <assert test="(             (name(@*) = 'nullFlavor' and 0 and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             (name(@*) != 'nullFlavor')              )">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/effectiveTime'"/> contient un 'nullFlavor' non autorisé ou porteur d'une valeur non admise.
        </assert>  
    </rule> 

    <rule context="cda:ClinicalDocument/cda:effectiveTime/cda:low">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/effectiveTime'"/>/low/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
    </rule>
    
    <rule context="cda:ClinicalDocument/cda:effectiveTime/cda:high">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/effectiveTime'"/>/high/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
     </rule>       

</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="healthcareFacilityTypeCode">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="cda:encompassingEncounter/cda:location/cda:healthCareFacility/cda:code">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="'componentOf/encompassingEncounter/location/healtCareFacility/code'"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="'componentOf/encompassingEncounter/location/healtCareFacility/code'"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_healthcareFacilityTypeCode)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="'componentOf/encompassingEncounter/location/healtCareFacility/code'"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_healthcareFacilityTypeCode"/>.
        </assert>
    </rule>
</pattern> 
    <!--Start pattern based on abstract personName--><pattern id="informantAssignedPersonName">
    <rule context="cda:informant/cda:assignedEntity/cda:assignedPerson/cda:name">
        <assert test="(                         (name(@*) = 'nullFlavor' and 1 and                            (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or                         ((./cda:family) and                        ((./cda:family[@qualifier='BR' or @qualifier='SP' or @qualifier='CL']) or not(./cda:family[@qualifier])))                     )">
            Erreur de conformité CI-SIS : L'élément <value-of select="'informant/assignedEntity/assignedPerson/name'"/>/family doit être présent 
            avec un attribut qualifier valorisé dans : BR (nom de famille), SP (nom d'usage) ou CL (pseudonyme)
            ou sans attribut qualifier. Valeur trouvée pour family : <value-of select="./cda:family"/>. Valeur trouvée pour family@qualifier : <value-of select="./cda:family/@qualifier"/>
        </assert>
    </rule>
</pattern>
    <pattern id="informantRelatedEntity">
    <rule context="cda:informant/cda:relatedEntity">
        <assert test="((name(@*) = 'classCode') and                          (@* = 'ECON' or @* = 'GUARD' or @* = 'POLHOLD' or @* = 'CON' or @* = 'QUAL')                     )">
            Erreur de conformité CI-SIS : L'élément informant/relatedEntity doit avoir un attribut classCode dont la valeur est dans l'ensemble :
            (ECON, GUARD, POLHOLD, CON, QUAL).
        </assert>
        <assert test="./cda:addr">
            Erreur de conformité CI-SIS : L'élément informant/relatedEntity doit comporter une adresse géographique (nullFlavor autorisé)
        </assert>
        <assert test="./cda:telecom">
            Erreur de conformité CI-SIS : L'élément informant/relatedEntity doit comporter une adresse telecom (nullFlavor autorisé)
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract personName--><pattern id="legalAuthenticatorName">
    <rule context="cda:legalAuthenticator/cda:assignedEntity/cda:assignedPerson/cda:name">
        <assert test="(                         (name(@*) = 'nullFlavor' and 0 and                            (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or                         ((./cda:family) and                        ((./cda:family[@qualifier='BR' or @qualifier='SP' or @qualifier='CL']) or not(./cda:family[@qualifier])))                     )">
            Erreur de conformité CI-SIS : L'élément <value-of select="'legalAuthenticator/assignedEntity/assignedPerson/name'"/>/family doit être présent 
            avec un attribut qualifier valorisé dans : BR (nom de famille), SP (nom d'usage) ou CL (pseudonyme)
            ou sans attribut qualifier. Valeur trouvée pour family : <value-of select="./cda:family"/>. Valeur trouvée pour family@qualifier : <value-of select="./cda:family/@qualifier"/>
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract IVL_TS--><pattern id="legalAuthenticatorTime">
    <p>
        Vérification de la conformité au CI-SIS d'un élément de type IVL_TS ou TS du standard CDAr2 :
        L'élément doit porter soit un attribut "value" soit un intervalle éventuellement semi-borné de sous-éléments "low", "high". 
        Alternativement, si l'attribut nullFlavor est autorisé, il doit porter l'une des valeurs admises par le CI-SIS. 
    </p>
    <rule context="cda:legalAuthenticator/cda:time">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'legalAuthenticator/time'"/>/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
        <assert test="(@* and not(*)) or (not(@*) and *)">
            Erreur de conformité CI-SIS : <value-of select="'legalAuthenticator/time'"/> doit contenir soit un attribut soit des éléments fils.
        </assert>
        <assert test="(             (name(@*) = 'nullFlavor' and 1 and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             (name(@*) != 'nullFlavor')              )">
            Erreur de conformité CI-SIS : <value-of select="'legalAuthenticator/time'"/> contient un 'nullFlavor' non autorisé ou porteur d'une valeur non admise.
        </assert>  
    </rule> 

    <rule context="cda:legalAuthenticator/cda:time/cda:low">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'legalAuthenticator/time'"/>/low/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
    </rule>
    
    <rule context="cda:legalAuthenticator/cda:time/cda:high">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'legalAuthenticator/time'"/>/high/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
     </rule>       

</pattern>
    <pattern id="modeleCommunEnTete">
    <p>Conformité de base de l'en-tête CDA au CI-SIS</p>
    <rule context="cda:ClinicalDocument">
        <assert test="cda:templateId[@root=$enteteHL7France]"> 
            Erreur de conformité HL7 France :
            L'élément ClinicalDocument/templateId doit être présent 
            avec @root = "<value-of select="$enteteHL7France"/>". 
        </assert>
        <assert test="cda:templateId[@root=$commonTemplate]"> 
            Erreur de conformité CI-SIS :
            L'élément ClinicalDocument/templateId doit être présent avec @root = "<value-of select="$commonTemplate"/>". 
        </assert>
        <assert test="cda:title and normalize-space(cda:title) and not(cda:title[@nullFlavor])">
            Erreur de conformité CI-SIS : 
            L'élément "title" doit être présent dans l'en-tête, 
            sans nullFlavor et doit contenir un titre non vide. 
        </assert>
        <assert test="cda:effectiveTime and not(cda:effectiveTime[@nullFlavor])"> 
            Erreur de conformité CI-SIS : 
            L'élément "effectiveTime" doit être présent dans l'en-tête, sans nullFlavor. 
        </assert>
        <assert test="cda:realmCode[@code='FR']"> 
            Erreur de conformité CI-SIS : 
            L'élément "realmCode" doit être présent et valorisé à "FR". 
        </assert>
        <assert test="not(cda:confidentialityCode[@nullFlavor])"> 
            Erreur de conformité CI-SIS :
            L'élément "confidentialityCode" (obligatoire dans CDAr2) doit être sans nullFlavor. 
        </assert>
        <assert test="cda:languageCode[@code='fr-FR']"> 
            Erreur de conformité CI-SIS : 
            L'élément "languageCode" doit être présent dans l'en-tête, valorisé à "fr-FR". 
        </assert>
        <assert test="not(cda:id[@nullFlavor])"> 
            Erreur de conformité CI-SIS : 
            L'élément "id", identifiant unique du document (obligatoire dans CDAr2) doit être sans nullFlavor. 
        </assert>
        <assert test="cda:legalAuthenticator and not(./cda:legalAuthenticator[@nullFlavor])">
            Erreur de conformité CI-SIS : 
            L'élément "legalAuthenticator" doit être présent dans l'en-tête, sans nullFlavor. 
        </assert>
        <assert test="not(cda:author[@nullFlavor]) and not(./cda:author/cda:assignedAuthor[@nullFlavor])"> 
            Erreur de conformité CI-SIS : 
            Les éléments "author" et "author/assignedAuthor" doivent être sans nullFlavor dans l'en-tête. 
        </assert>
        <assert test="not(cda:custodian[@nullFlavor]) and not(./cda:custodian/cda:assignedCustodian[@nullFlavor])"> 
            Erreur de conformité CI-SIS : 
            Les éléments "custodian" et "custodian/assignedCustodian" doivent être sans nullFlavor dans l'en-tête. 
       </assert>
        <assert test="(cda:documentationOf) and not(cda:documentationOf[@nullFlavor]) and                     not(cda:documentationOf/cda:serviceEvent[@nullFlavor])"> 
            Erreur de conformité CI-SIS : 
            L'en-tête doit comporter au moins un élément documentationOf
            et l'attribut nullFlavor n'est pas autorisé ni sur documentationOf ni sur son fils serviceEvent. 
        </assert>
        <assert test="cda:componentOf">
            Erreur de conformité CI-SIS : 
            Le document doit comporter dans son en-tête un componentOf/encompassingEncounter.
        </assert>
        <assert test="cda:componentOf/cda:encompassingEncounter/cda:effectiveTime/@nullFlavor or                     cda:componentOf/cda:encompassingEncounter/cda:effectiveTime/cda:low/@value or                     cda:componentOf/cda:encompassingEncounter/cda:effectiveTime/cda:high/@value             ">
            Erreur de conformité CI-SIS : 
            L'élément componentOf/encompassingEncounter/effectiveTime doit comporter 
            soit un attribut nullFlavor soit l'un des éléments fils "low/@value" et "high/@value" 
            soit les deux.
        </assert>
        <assert test="cda:componentOf/cda:encompassingEncounter/cda:location/cda:healthCareFacility/cda:code">
            Erreur de conformité CI-SIS : 
            Le document doit comporter dans son en-tête un componentOf/encompassingEncounter/location/healthCareFacility/code.
        </assert>
    </rule>
</pattern>
    <pattern id="patient">
    <rule context="cda:ClinicalDocument/cda:recordTarget/cda:patientRole">
        <assert test="not(cda:id[@nullFlavor])">
            Erreur de conformité CI-SIS : L'élément recordTarget/patientRole/id (obligatoire dans CDAr2), 
            doit être sans nullFlavor.
        </assert>
        <assert test="cda:id[@root=$OIDINS-c]">
            Erreur de conformité CI-SIS : l'élément recordTarget/patientRole 
            doit comporter au moins un élément id contenant un INS-c du patient
        </assert>
        <assert test="cda:patient/cda:birthTime">
            Erreur de conformité CI-SIS : l'élément recordTarget/patientRole/patient/birthTime doit être présent 
            avec une date de naissance ou un nullFlavor autorisé.
        </assert>
        <assert test="cda:addr">
            Erreur de conformité CI-SIS : l'élément recordTarget/patientRole/addr doit être présent 
            avec une adresse géographique ou un nullFlavor autorisé.
        </assert>
        <assert test="cda:telecom">
            Erreur de conformité CI-SIS : l'élément recordTarget/patientRole/telecom doit être présent 
            avec une adresse de télécommunication ou un nullFlavor autorisé.
        </assert>
        <assert test="cda:patient/cda:administrativeGenderCode">
            Erreur de conformité CI-SIS : l'élément recordTarget/patientRole/patient/administrativeGenderCode 
            doit être présent avec le code sexe ou un nullFlavor autorisé.
        </assert>
        <assert test="                     not(cda:patient/cda:religiousAffiliationCode) and                     not(cda:patient/cda:raceCode) and                     not(cda:patient/cda:ethnicGroupCode)                      ">
            Erreur de conformité CI-SIS : Un élément recordTarget/patientRole/patient 
            ne doit contenir ni race ni religion ni groupe ethnique.
        </assert>
        <assert test="cda:patient/cda:name/cda:given">
            Erreur de conformité CI-SIS : l'élément recordTarget/patientRole/patient/name/given doit être présent avec le prénom du patient ou un nullFlavor.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract IVL_TS--><pattern id="patientBirthTime">
    <p>
        Vérification de la conformité au CI-SIS d'un élément de type IVL_TS ou TS du standard CDAr2 :
        L'élément doit porter soit un attribut "value" soit un intervalle éventuellement semi-borné de sous-éléments "low", "high". 
        Alternativement, si l'attribut nullFlavor est autorisé, il doit porter l'une des valeurs admises par le CI-SIS. 
    </p>
    <rule context="cda:patient/cda:birthTime">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'patient/birthTime'"/>/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
        <assert test="(@* and not(*)) or (not(@*) and *)">
            Erreur de conformité CI-SIS : <value-of select="'patient/birthTime'"/> doit contenir soit un attribut soit des éléments fils.
        </assert>
        <assert test="(             (name(@*) = 'nullFlavor' and 1 and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             (name(@*) != 'nullFlavor')              )">
            Erreur de conformité CI-SIS : <value-of select="'patient/birthTime'"/> contient un 'nullFlavor' non autorisé ou porteur d'une valeur non admise.
        </assert>  
    </rule> 

    <rule context="cda:patient/cda:birthTime/cda:low">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'patient/birthTime'"/>/low/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
    </rule>
    
    <rule context="cda:patient/cda:birthTime/cda:high">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'patient/birthTime'"/>/high/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
     </rule>       

</pattern>
    <pattern id="patientId">
    <p>
        Vérification de la conformité au CI-SIS :
        l'INS-C doit être une chaîne de 22 chiffres 
    </p>
    <rule context="cda:ClinicalDocument/cda:recordTarget/cda:patientRole/cda:id">
        <assert test="             (@root = $OIDINS-c and string-length(@extension) = 22 and number(@extension) &gt; 1)               or (@root != $OIDINS-c)">
            Erreur de conformité CI-SIS : L'INS-c doit contenir une chaine de 22 chiffres 
            (valeur trouvée dans le document : <value-of select="@extension"/>,
             OID trouvé : <value-of select="@root"/>)
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract personName--><pattern id="patientName">
    <rule context="cda:patient/cda:name">
        <assert test="(                         (name(@*) = 'nullFlavor' and 0 and                            (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or                         ((./cda:family) and                        ((./cda:family[@qualifier='BR' or @qualifier='SP' or @qualifier='CL']) or not(./cda:family[@qualifier])))                     )">
            Erreur de conformité CI-SIS : L'élément <value-of select="'patient/name'"/>/family doit être présent 
            avec un attribut qualifier valorisé dans : BR (nom de famille), SP (nom d'usage) ou CL (pseudonyme)
            ou sans attribut qualifier. Valeur trouvée pour family : <value-of select="./cda:family"/>. Valeur trouvée pour family@qualifier : <value-of select="./cda:family/@qualifier"/>
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="practiceSettingCode">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="cda:serviceEvent/cda:performer/cda:assignedEntity/cda:representedOrganization/cda:standardIndustryClassCode">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="'serviceEvent/performer/assignedEntity/representedOrganization/standardIndustryClassCode'"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="'serviceEvent/performer/assignedEntity/representedOrganization/standardIndustryClassCode'"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_practiceSettingCode)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="'serviceEvent/performer/assignedEntity/representedOrganization/standardIndustryClassCode'"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_practiceSettingCode"/>.
        </assert>
    </rule>
</pattern>
    <pattern id="relatedDocument">
    <p>
        Si l'élément relatedDocument est présent alors son attribut typeCode doit valoir RPLC 
    </p>
    <rule context="cda:relatedDocument">      
        <assert test="(count(@*) = 1 and name(@*) = 'typeCode' and @* = 'RPLC')">
            Erreur de conformité CI-SIS : ClinicalDocument/relatedDocument ne contient pas l'attribut typeCode avec la seule valeur autorisée : RPLC.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract personName--><pattern id="relatedPersonName">
    <rule context="cda:relatedPerson/cda:name">
        <assert test="(                         (name(@*) = 'nullFlavor' and 1 and                            (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or                         ((./cda:family) and                        ((./cda:family[@qualifier='BR' or @qualifier='SP' or @qualifier='CL']) or not(./cda:family[@qualifier])))                     )">
            Erreur de conformité CI-SIS : L'élément <value-of select="'relatedPerson/name'"/>/family doit être présent 
            avec un attribut qualifier valorisé dans : BR (nom de famille), SP (nom d'usage) ou CL (pseudonyme)
            ou sans attribut qualifier. Valeur trouvée pour family : <value-of select="./cda:family"/>. Valeur trouvée pour family@qualifier : <value-of select="./cda:family/@qualifier"/>
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract IVL_TS--><pattern id="serviceEventEffectiveTime">
    <p>
        Vérification de la conformité au CI-SIS d'un élément de type IVL_TS ou TS du standard CDAr2 :
        L'élément doit porter soit un attribut "value" soit un intervalle éventuellement semi-borné de sous-éléments "low", "high". 
        Alternativement, si l'attribut nullFlavor est autorisé, il doit porter l'une des valeurs admises par le CI-SIS. 
    </p>
    <rule context="cda:ClinicalDocument/cda:documentationOf/cda:serviceEvent/cda:effectiveTime">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/documentationOf/serviceEvent/effectiveTime'"/>/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
        <assert test="(@* and not(*)) or (not(@*) and *)">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/documentationOf/serviceEvent/effectiveTime'"/> doit contenir soit un attribut soit des éléments fils.
        </assert>
        <assert test="(             (name(@*) = 'nullFlavor' and 1 and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             (name(@*) != 'nullFlavor')              )">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/documentationOf/serviceEvent/effectiveTime'"/> contient un 'nullFlavor' non autorisé ou porteur d'une valeur non admise.
        </assert>  
    </rule> 

    <rule context="cda:ClinicalDocument/cda:documentationOf/cda:serviceEvent/cda:effectiveTime/cda:low">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/documentationOf/serviceEvent/effectiveTime'"/>/low/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
    </rule>
    
    <rule context="cda:ClinicalDocument/cda:documentationOf/cda:serviceEvent/cda:effectiveTime/cda:high">
        <let name="L" value="string-length(@value)"/>
        <let name="mm" value="number(substring(@value,5,2))"/>
        <let name="dd" value="number(substring(@value,7,2))"/>            
        <let name="hh" value="number(substring(@value,9,2))"/>
        <let name="lzp" value="string-length(substring-after(@value,'+'))"/> 
        <let name="lzm" value="string-length(substring-after(@value,'-'))"/>
        <let name="lDH1" value="string-length(substring-before(@value,'+'))"/>
        <let name="lDH2" value="string-length(substring-before(@value,'-'))"/>
        <assert test="(             ($L = 0) or             ($L = 4) or              ($L = 6 and $mm &lt;= 12) or              ($L = 8 and $mm &lt;= 12 and $dd &lt;= 31) or              ($L &gt; 14 and $mm &lt;= 12 and $dd &lt;= 31 and $hh &lt; 24 and ($lzp = 4 or $lzm = 4) and $lDH1 &lt;= 14 and $lDH2 &lt;= 14)             )">
            Erreur de conformité CI-SIS : <value-of select="'ClinicalDocument/documentationOf/serviceEvent/effectiveTime'"/>/high/@value = "<value-of select="@value"/>"  contient  
            une date et heure invalide, différente de aaaa ou aaaamm ou aaaammjj ou aaaammjjhh[mm[ss]][+/-]zzzz 
            en temps local du producteur.
        </assert>
     </rule>       

</pattern>
    <pattern id="serviceEventPerformer">
    <p>
        Vérification de la présence et de la conformité de l'acte principal documenté 
    </p>
    <rule context="cda:ClinicalDocument">
        
        <assert test="count(cda:documentationOf/cda:serviceEvent/cda:performer) = 1">
            Erreur de conformité CI-SIS : l'en-tête CDA doit comporter un et un seul documentationOf/serviceEvent 
            avec un élément fils performer. </assert>
        
    </rule>
    
    <rule context="cda:ClinicalDocument/cda:documentationOf/cda:serviceEvent/cda:performer">
        
        <assert test="not(@nullFlavor)">
            Erreur de conformité CI-SIS : L'élément documentationOf/serviceEvent/performer doit être renseigné sans nullFlavor. </assert>
        
        <assert test="../cda:effectiveTime/cda:low and                        not(../cda:effectiveTime[@nullFlavor]) and                       not(../cda:effectiveTime/cda:low[@nullFlavor])">
            Erreur de conformité CI-SIS : L'élément documentationOf/serviceEvent portant l'acte principal documenté
            doit comporter à la fois un fils performer et un petit-fils effectiveTime/low sans attribut nullFlavor. </assert>
        
        <assert test="cda:assignedEntity/cda:representedOrganization/cda:standardIndustryClassCode">
            Erreur de conformité CI-SIS : L'élément documentationOf/serviceEvent/performer correspondant à l'acte principal documenté, 
            doit comporter un descendant assignedEntity/representedOrganization/standardIndustryClassCode. </assert>
        
    </rule>
</pattern>
    <pattern id="telecom">
    <p>
        Vérification de la conformité au CI-SIS d'un élément telecom (de type TEL) du standard CDAr2 :
        L'élément doit comporter un attribut "value" bien formaté avec les préfixes autorisés par le CI-SIS, 
        et optionnellement un attribut "use" (qui n'est pas contrôlé).
        Alternativement, si l'attribut nullFlavor est présent, il doit avoir l'une des valeurs admises par le CI-SIS. 
    </p>
    <rule context="cda:telecom">
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>           
        <assert test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">
            Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.
        </assert>
    </rule>
</pattern>    
    <pattern id="CS8ModeleEnTete">
    <p>Conformité de l'en-tête CDA au modèle du CS8</p>
    <rule context="cda:ClinicalDocument">
        <assert test="./cda:templateId[@root='1.2.250.1.213.1.1.1.5.1']"> 
            Erreur de conformité CS8 :
            L'élément ClinicalDocument/templateId doit être présent 
            avec @root="1.2.250.1.213.1.1.1.5.1".</assert>
        <assert test="cda:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.1']"> 
            Erreur: Le template parent "QRPH Health birth summary" (1.3.6.1.4.1.19376.1.7.3.1.1.13.1) doit être présent.
        </assert>
        <assert test="cda:templateId[@root='1.2.250.1.213.1.1.1.5']"> 
            Erreur: Le template parent "Certificat de Santé de l'Enfant" (1.2.250.1.213.1.1.1.5) doit être présent.
        </assert>
        
        <assert test="./cda:code[@code='CERT_DECL' and @codeSystem='1.2.250.1.213.1.1.4.12']"> 
            Erreur de conformité CS8 : 
            L'élément code doit avoir @code ="CERT_DECL" et @codeSystem = "1.2.250.1.213.1.1.4.12"/&gt;. </assert>
    </rule>
</pattern> 
    
    
                                    
    
    
    <pattern id="CodedAntenatalTestingAndSurveillance-errors">
    <title>IHE PCC v3.0 Coded Antenatal Testing and Surveillance Section</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.21.2.5.1&#34;]"> 
        
                
        <assert test="../cda:section"> 
            [CodedAntenatalTestingAndSurveillance] 'Coded Antenatal Testing and Surveillance' ne peut être utilisé que comme section.
        </assert> 

         
        <assert test="cda:code[@code = &#34;57078-8&#34;]"> 
            [CodedAntenatalTestingAndSurveillance] Le code de la section 'Prenatal Events' doit être '57078-8'
        </assert> 
        
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            [CodedAntenatalTestingAndSurveillance] L'élément 'codeSystem' de la section 
            'Coded Antenatal Testing and Surveillance Section' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1). 
        </assert>
        
         
        <assert test="cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.21.2.5&#34;]"> 
            [CodedAntenatalTestingAndSurveillance] L'OID du template parent de la section 'Coded physical Exam' est absent. 
        </assert> 
        
        
        <assert test=".//cda:templateId[@root = &#34;1.3.6.1.4.1.19376.1.5.3.1.1.21.3.10&#34;]">
            [CodedAntenatalTestingAndSurveillance] Une section 'Antenatal Testing and Surveillance' doit contenir un élément 'Antenatal Testing and Surveillance Battery'.
        </assert> 
    </rule>
</pattern>
    <pattern id="codedPhysicalExam-errors">
    <title>IHE PCC v3.0 Physical Exam Section - errors validation phase</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1&#34;]"> 
                
        <assert test="../cda:section"> 
            Erreur de Conformité PCC: 'Coded physical Exam' ne peut être utilisé que comme section.
        </assert> 
         
        <assert test="cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.15&#34;]"> 
            Erreur de Conformité PCC: L'OID du template parent de la section 'Coded physical Exam' est absent. 
        </assert> 
        <assert test="cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.24&#34;]"> 
            Erreur de Conformité au volet CSE: L'OID du template parent de la section 'Coded physical Exam' est absent. 
        </assert>
         
        <assert test="cda:code[@code = &#34;29545-1&#34;]"> 
            Erreur de Conformité au volet CSE: Le code de la section 'Coded physical Exam' doit être '29545-1'
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            Erreur de Conformité au volet CSE: L'élément 'codeSystem' de la section 'Coded physical exam' doit être codé dans la nomenclature LOINC 
            (2.16.840.1.113883.6.1). 
        </assert> 
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    </rule>
</pattern>
    <pattern id="codedResults-errors">

    <title>IHE PCC Coded Results Section</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.28&#34;]">
         
        <assert test="../cda:section"> 
            [codedResults] 'Coded Results' ne peut être utilisé que comme section.</assert>
        
        <assert test="cda:code[@code = '30954-2']"> 
            [codedResults] Le code de la section 'Coded Results' doit être '30954-2'</assert>
        
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            [codedResults] L'élément 'codeSystem' de la section 
            'Coded Results' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1).</assert>
    </rule>
</pattern>
    <pattern id="codedSocialHistory-errors">
   
    <title>IHE PCC v3.0 Coded Social History Section</title>
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.16.1&#34;]"> 
         
        <assert test="../cda:section"> 
            [codedSocialHistory] le templateId de 'Coded Social History' ne peut être utilisé que pour une section.
        </assert> 
         
        <assert test="cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.16&#34;]"> 
            [codedSocialHistory] L'OID du template parent de la section 'Coded Social History' est absent. 
        </assert> 
         
        <assert test="cda:code[@code = &#34;29762-2&#34;]"> 
            [codedSocialHistory] Le code de la section 'Coded Social History' doit être 29762-2
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            [codedSocialHistory] L'attribut 'codeSystem' de la section 'Coded Social History' doit être codé dans la nomenclature LOINC 
            (2.16.840.1.113883.6.1). 
        </assert> 
        <assert test=".//cda:templateId[@root = &#34;1.3.6.1.4.1.19376.1.5.3.1.4.13.4&#34;]"> 
            
            [codedSocialHistory] La section "Coded Social History"  doit contenir des éléments d'entrée "Social History Observation".
        </assert> 
    </rule>
</pattern>
    <pattern id="EarsPhysicalExam-errors">
    <title>IHE PCC v3.0 Ears Section</title>
    
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.21&#34;]">
        
        <assert test="../cda:section"> 
            Erreur de Conformité PCC: Ce template ne peut être utilisé que pour une section.
        </assert>         
         
        <assert test="cda:code[@code = &#34;10195-6&#34;]"> 
            Erreur de Conformité PCC: Le code de la section 'Ears' doit être 10195-6
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            Erreur de Conformité PCC: L'élément 'codeSystem' de la section 'Ears' 
            doit être codé à partir de la nomenclature LOINC 
            (2.16.840.1.113883.6.1). 
        </assert> 
    </rule>
</pattern>
    <pattern id="carePlan-errors">
    <title>IHE PCC v3.0 Care Plan Section</title>
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.31&#34;]"> 
         
        <assert test="../cda:section"> 
            [carePlan] 'Care Plan' ne peut être utilisé que comme section. 
        </assert> 
         
        <assert test="cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.10&#34;]"> 
            [carePlan] L'OID du template parent de la section 'Care Plan' (2.16.840.1.113883.10.20.1.10) est absent. 
        </assert> 
         
        <assert test="cda:code[@code = &#34;18776-5&#34;]"> 
            [carePlan] Le code de la section 'Care Plan' doit être '18776-5' 
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            [carePlan] L'attribut 'codeSystem' de la section a pour valeur '2.16.840.1.113883.6.1' (LOINC)  
            system (). 
        </assert> 
        
        
        
        
        
    </rule>
    
</pattern>
    <pattern id="codedVitalSigns-errors">
    <title>IHE PCC v3.0 Coded Vital Signs Section - errors validation phase</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2&#34;]">
        

        <assert test="../cda:section"> 
            [codedVitalSigns] ce template ne peut être utilisé que comme section.
        </assert>         
         
        <assert test="cda:code[@code = &#34;8716-3&#34;]"> 
            [codedVitalSigns] Le code de la section Coded Vital signs doit être 8716-3
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            [codedVitalSigns] L'élément 'codeSystem' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1). 
        </assert>
         
        <assert test="cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.25&#34;]"> 
            [codedVitalSigns] L'identifiant du template parent pour la section est absent. 
        </assert>
        
        <assert test=".//cda:templateId[@root = &#34;1.3.6.1.4.1.19376.1.5.3.1.4.13.1&#34;]">
            [codedVitalSigns] Une section 'Coded Vital Signs' doit contenir un élément 'Vital Signs Organizer'.
        </assert> 
    </rule>
</pattern>
    <pattern id="activeProblemSection-errors">


    <title>IHE PCC v3.0 Active Problems Section</title>

    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.6&#34;]">
        
        <assert test="../cda:section"> 
            [activeProblemSection] : 'Active Problems' ne peut être utilisé que comme section.</assert>
        
        
        <assert test="cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.11&#34;]"> 
            [activeProblemSection] : Le templateId parent de la section 'Active Problems' (2.16.840.1.113883.10.20.1.11) doit être présent</assert>
        
        
        <assert test="cda:code[@code = &#34;11450-4&#34;]"> 
            [activeProblemSection] : Le code de la section 'Active Problems' doit être '11450-4'</assert>
        
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            [activeProblemSection] : L'élément 'codeSystem' de la section 
            'Active Problems' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1)</assert>
        

        
        <assert test=".//cda:text">
            [activeProblemSection] : Une section "Active Problems" doit contenir un élément text"</assert>
    </rule>
</pattern>
    <pattern id="assessmentAndPlan-errors">
    <title>IHE PCC v3.0 Assessment and Plan Section</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5&#34;]"> 
         
        <assert test="../cda:section"> 
            Erreur de Conformité volet PCC: 'Assessment and Plan' ne peut être utilisé que comme section. 
        </assert> 
         
        <assert test="cda:code[@code = &#34;51847-2&#34;]"> 
            Erreur de Conformité volet PCC: Le code de la section 'Assessment and Plan' doit être 51847-2 
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            Erreur de Conformité volet PCC: L'élément 'codeSystem' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1). 
        </assert> 
    </rule>   
</pattern>
    <pattern id="encounterHistoriesSection-errors">
    <title>IHE PCC v3.0 Encounter Histories Section</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.5.3.3&#34;]">
        
        <assert test="../cda:section"> 
        Erreur de Conformité PCC: 'Encounter Histories' ne peut être utilisé que comme section.</assert>
        
        
        <assert test="cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.3&#34;]"> 
        Erreur de Conformité PCC: Les templateId des parents doivent être présents. </assert>
        
        
        <assert test="cda:code[@code = &#34;46240-8&#34;]"> 
        Erreur de Conformité PCC: Le code de la section 'Encounter Histories' doit être '46240-8'</assert>
        
        <assert test="cda:code[@codeSystem = '2.16.840.1.113883.6.1']"> 
            Erreur de Conformité PCC: L'élément 'codeSystem' de la section 
            'Encounter Histories' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1).</assert>
        
        
        <assert test=".//cda:templateId[@root = &#34;1.3.6.1.4.1.19376.1.5.3.1.4.14&#34;]">
            Erreur de Conformité PCC: Une section "Encounter Histories" doit contenir des entrée de type "Encounters".</assert>

    </rule>
</pattern>
    <pattern id="generalAppearancePhysicalExam-errors">
    <title>IHE PCC v3.0 General appearance Section</title>
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.16&#34;]">
        
        <assert test="../cda:section"> 
            Erreur de Conformité PCC: Cet élément ne peut être utilisé que comme section.
        </assert>         
         
        <assert test="cda:code[@code = &#34;10210-3&#34;]"> 
            Erreur de Conformité PCC: Le code de la section Système cutané doit être 10210-3
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            Erreur de Conformité PCC: L'élément 'codeSystem' doit être codé dans la nomenclature LOINC 
            (2.16.840.1.113883.6.1). 
        </assert>
        
    </rule>
</pattern>
    <pattern id="genitaliaPhysicalExam-errors">
    <title>IHE PCC v3.0 Genitalia</title>
    

    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.36&#34;]">
            
            <assert test="../cda:section"> 
                Erreur de Conformité PCC: Cet élément ne peut être utilisé que comme section.
            </assert>         
             
            <assert test="cda:code[@code =&#34;11400-9&#34;]"> 
                Erreur de Conformité PCC: Le code de la section doit être 11400-9
            </assert> 
            <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
                Erreur de Conformité PCC: L'élément 'codeSystem' doit être codé dans la nomenclature LOINC 
                (2.16.840.1.113883.6.1). 
            </assert>
        </rule>

</pattern>
    <pattern id="heartPhysicalExam-errors">
    <title>IHE PCC v3.0 Heart Section</title>
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.29&#34;]">
        
        <assert test="../cda:section"> 
            Erreur de Conformité PCC: L'entité 'Système Cardiaque' ne peut être utilisé que comme section.
        </assert>         
         
        <assert test="cda:code[@code = &#34;10200-4&#34;]"> 
            Erreur de Conformité PCC: Le code de la section 'Système Cardiaque' doit être 10200-4
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            Erreur de Conformité PCC: L'attribut 'codeSystem' de la section 'Système Cardiaque'doit être codé dans la nomenclature LOINC 
            (2.16.840.1.113883.6.1). 
        </assert>
    </rule>
</pattern>
    <pattern id="historyOfPastIllness-errors">
    <title>IHE PCC v3.0 History of Past Illness Section</title>
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.8&#34;]"> 
         
        <assert test="../cda:section"> 
            [historyOfPastIllness] History of Past Illness ne peut être utilisé que dans une section. 
        </assert> 
         
        <assert test="cda:code[@code = &#34;11348-0&#34;]"> 
            [historyOfPastIllness] Le code de la section History of Past Illness doit être 11348-0 
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            [historyOfPastIllness] L'élément 'codeSystem' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1). 
        </assert> 
        <assert test=".//cda:templateId[@root = &#34;1.3.6.1.4.1.19376.1.5.3.1.4.5.2&#34;]"> 
            
            [historyOfPastIllness] History of Past Illness doit contenir des éléments Problem Concern Entry.
        </assert> 
    </rule> 
</pattern>
    <pattern id="immunizations-errors">
    <title>IHE PCC v3.0 Immunizations Section</title>
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.23&#34;]"> 
         
        <assert test="../cda:section"> 
            Erreur de Conformité PCC: Immunizations ne peut être utilisé que comme section.
        </assert> 
         
        <assert test="cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.6&#34;]"> 
            Erreur de Conformité PCC: L'OID de l'élément parent n'est pas présent.
        </assert> 
         
        <assert test="cda:code[@code = &#34;11369-6&#34;]"> 
            Erreur de Conformité PCC: Le code de la section doit être 11369-6 
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            Erreur de Conformité PCC: L'élément 'codeSystem' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1). 
        </assert> 
        
        <assert test=".//cda:templateId[@root = &#34;1.3.6.1.4.1.19376.1.5.3.1.4.12&#34;]"> 
            Erreur de Conformité PCC: Une section Immunizations doit contenir au moins une entrée Immunization.           
        </assert> 
    </rule>         
</pattern>
    <pattern id="laborAndDeliverySection-errors">
    <title>IHE PCC Labor and Delivery section</title>

<rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3&#34;]"> 
     
    <assert test="../cda:section"> 
        [laborAndDeliverySection] le templateId de 'Labor and Delivery' ne peut être utilisé que pour une section. 
    </assert> 
 
    
    <assert test="cda:code[@code = &#34;57074-7&#34;]"> 
        [laborAndDeliverySection] Le code utilisé pour la section "Labor and Delivery" doit être "57074-7" 
            </assert> 
    <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
        [laborAndDeliverySection] La nomenclature LOINC doit être utilisée pour coder la section "Labor and Delivery"  
        system (2.16.840.1.113883.6.1). 
    </assert> 
    
    
        </rule> 
        
    </pattern>
    <pattern id="musculoPhysicalExam-errors">
    <title>IHE PCC v3.0 Care Plan Section - errors validation phase</title>
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.34&#34;]">
        
        <assert test="../cda:section"> 
            Erreur de Conformité PCC: l'élément 'Musculoskeletal system' ne peut être utilisé que comme section.
        </assert>         
         
        <assert test="cda:code[@code =&#34;11410-8&#34;]"> 
            Erreur de Conformité PCC: Le code de la section 'Musculoskeletal system' doit être 11410-8
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            Erreur de Conformité PCC: L'attribut 'codeSystem' de la section 'Musculoskeletal system' doit être codé dans la nomenclature LOINC 
            (2.16.840.1.113883.6.1). 
        </assert>

    </rule>
</pattern>
    <pattern id="neurologicPhysicalExam-errors">
    <title>IHE PCC v3.0 Neurologic System</title>
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.35&#34;]">
        
        <assert test="../cda:section"> 
            Erreur de Conformité PCC: "Neurologic System" ne peut être utilisé que comme section.
        </assert>         
         
        <assert test="cda:code[@code =&#34;10202-0&#34;]"> 
            Erreur de Conformité PCC: Le code de la section "Neurologic System" doit être 10202-0
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            Erreur de Conformité PCC: L'attribut 'codeSystem' de la section "Neurologic System" doit être codé dans la nomenclature LOINC 
            (2.16.840.1.113883.6.1). 
        </assert>
        
    </rule>
    
</pattern>
    <pattern id="pregnancyHistorySection-errors">
    <title>IHE PCC v3.0 Pregnancy History Section</title>
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.5.3.4&#34;]"> 
         
        <assert test="../cda:section"> 
            [pregnancyHistorySection] Le templateId de 'Pregnancy History' ne peut être utilisé que pour une section. 
        </assert> 
        
         
        <assert test="cda:code[@code = &#34;10162-6&#34;]"> 
            [pregnancyHistorySection] Le code utilisé pour la section "Pregnancy History" doit être "10162-6" 
        </assert> 
        
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            [pregnancyHistorySection] La nomenclature LOINC doit être utilisée pour coder la section "Pregnancy History"  
            system (2.16.840.1.113883.6.1). 
        </assert> 
        
        <assert test=".//cda:templateId[@root =&#34;1.3.6.1.4.1.19376.1.5.3.1.4.13.5&#34;]">
            [pregnancyHistorySection] Une section Pregnancy History doit comporter des entrées de type Pregnancy Observation</assert> 
    </rule>
</pattern>
    <pattern id="prenatalEvents-errors">
    <title>IHE PCC v3.0 Prenatal Events Section</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.21.2.2&#34;]"> 
                
        <assert test="../cda:section"> 
            Erreur de Conformité au volet CSE: 'Prenatal Events' ne peut être utilisé que comme section.
        </assert> 

         
        <assert test="cda:code[@code = &#34;57073-9&#34;]"> 
            Erreur de Conformité au volet CSE: Le code de la section 'Prenatal Events' doit être '57073-9'
        </assert> 
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
            Erreur de Conformité au volet CSE: L'élément 'codeSystem' de la section 
            'Prenatal Events' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1). 
        </assert>
        
        
        <assert test=".//cda:templateId[@root =&#34;1.3.6.1.4.1.19376.1.5.3.1.3.28&#34;]">
            Erreur de Conformité au volet CSE: La section 'Prenatal Events' ne contient pas de sous-section'Coded Results'.
        </assert> 
        
        
    </rule>
</pattern>
    <pattern id="proceduresSection-errors">
    <title>IHE PCC v3.0 Procedures Section</title>

        <title>IHE PCC v3.0 Procedures Section</title>
        
        
        <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.13.2.11&#34;]"> 
             
            <assert test="../cda:section"> 
                Erreur de Conformité PCC: 'Procedures' ne peut être utilisé que comme section
            </assert> 
             
            <assert test="cda:code[@code = &#34;29544-3&#34;]"> 
                Erreur de Conformité PCC: Le code de la section 'Procedures' doit être '29544-3'              
            </assert> 
            <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]"> 
                L'élément 'codeSystem' de la section 
                'Procedures' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1)
            </assert> 
            
            <assert test=".//cda:templateId[@root = &#34;1.3.6.1.4.1.19376.1.5.3.1.4.19&#34;]"> 
                
                Erreur de Conformité PCC: Une section "Procedures and Interventions" doit contenir des entrée de type "Procedures entry"
            </assert> 
        </rule> 
        
    </pattern>
    
                                
    
    <pattern id="CS8codedPhysicalExam-errors">
    <title>ASIP CI-SIS CS8 Physical Exam Section</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1&#34;]"> 
        
        <assert test=".//cda:templateId[@root =&#34;1.3.6.1.4.1.19376.1.5.3.1.1.5.3.2&#34;]">
            Erreur de Conformité au volet CSE: La section 'Coded physical exam' ne contient pas de sous-section'Coded Vital Signs'.
        </assert>
        
        
        <assert test=".//cda:templateId[@root =&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.16&#34;]">
            Erreur de Conformité au volet CSE: La section 'Coded physical exam' ne contient pas de sous-section'General Appearance'.
        </assert>
        
        <assert test=".//cda:templateId[@root =&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.21&#34;]">
            Erreur de Conformité au volet CSE: La section 'Coded physical exam' ne contient pas de sous-section 'Ears'.
        </assert> 
        
        <assert test=".//cda:templateId[@root =&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.29&#34;]">
            Erreur de Conformité au volet CSE: La section 'Coded physical exam' ne contient pas de sous-section 'Heart'.
        </assert> 
        
        
        <assert test=".//cda:templateId[@root =&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.34&#34;]">
            Erreur de Conformité au volet CSE: La section 'Coded physical exam' ne contient pas de sous-section 'Musculoskeletal System'.
        </assert> 
        
        <assert test=".//cda:templateId[@root =&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.35&#34;]"> 
            Erreur de Conformité au volet CSE: La section 'Coded physical exam' ne contient pas de sous-section 'Neurologic System'.
        </assert>
        
        <assert test=".//cda:templateId[@root =&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.36&#34;]">
            Erreur de Conformité au volet CSE: La section 'Coded physical exam' ne contient pas de sous-section 'Genitalia'.
        </assert>
    </rule>
</pattern>
    
    <pattern id="CSEcodedResultsSubsections-errors">
    

    <title>IHE PCC Coded Results Section</title>
    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.28']">

 
        <assert test=".//cda:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.19']">
            Erreur de Conformité au volet CSE: La section Coded Results doit contenir une entrée "Procedure". </assert>
    </rule>
</pattern>
    
                                    
    
    <pattern id="codedVitalSignsOrg-errors">
    <title>IHE PCC v3.0 Vital Signs Organizer</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.13.1&#34;]">
        <assert test="../cda:organizer"> 
            Erreur de Conformité PCC: 'Coded physical Exam' ne peut être utilisé que comme section.
        </assert> 
        
        <assert test="cda:code[@code =&#34;F-03400&#34;]">
            Erreur de Conformité PCC: Le codage de l'élément 'Vital Signs Organizer' doit être 'F-03400'.           
        </assert>
        
        <assert test="cda:code[@codeSystem =&#34;1.2.250.1.213.2.12&#34;]"> 
            Erreur de Conformité PCC: L'attribut 'codeSystem' de l'élément 'Vital Signs Organizer' a pour valeur '1.2.250.1.213.2.12' (SNOMED 3.5)           
        </assert>
         
        <assert test="cda:templateId[@root = &#34;2.16.840.1.113883.10.20.1.32&#34;]"> 
            Erreur de Conformité PCC: L'identifiant du template parent (2.16.840.1.113883.10.20.1.32) doit être présent. 
        </assert>
         
        <assert test="cda:templateId[@root = &#34;2.16.840.1.113883.10.20.1.35&#34;]"> 
            Erreur de Conformité PCC: L'identifiant du template parent (2.16.840.1.113883.10.20.1.35) doit être présent. 
        </assert>        
        
        <assert test=".//cda:templateId[@root = &#34;1.3.6.1.4.1.19376.1.5.3.1.4.13.2&#34;]"> 
            Erreur de Conformité PCC: L'élément 'Vital Sign Organizer' doit au moins contenir une entrée 'Vital Sign Observation'
        </assert> 

    </rule>
    

</pattern>
    <pattern id="codedAntenatalTestingAndSurveillanceOrg-errors">
    <title>IHE PCC v3.0 Coded Antenatal Testing and Surveillance Organizer</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.21.3.10&#34;]">
        <assert test="../cda:organizer"> 
            Erreur de Conformité PCC: 'Conformité PCC v3.0 (Erreur):' ne peut être utilisé que comme organizer.
        </assert> 
        
        <assert test="cda:code[@code=&#34;XX-ANTENATALTESTINGBATTERY&#34; and              @displayName=&#34;ANTENATAL TESTING AND SURVEILLANCE BATTERY&#34; and             @codeSystem=&#34;2.16.840.1.113883.6.1&#34; and             @codeSystemName=&#34;LOINC&#34;]">
            [codedAntenatalTestingAndSurveillanceOrg] L'élément &lt;code&gt; de l'organizer "Antenatal Testing and Surveillance"est requis, et 
            identifie celui-ci comme un organizer contenant des données de test et de surveillance: &lt;code code='XX-ANTENATALTESTINGBATTERY'
            displayName='ANTENATAL TESTING AND SURVEILLANCE BATTERY' codeSystem='2.16.840.1.113883.6.1' codeSystemName="LOINC"</assert>
         
        
        <assert test="cda:component/cda:observation/cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.13&#34;]"> 
            [codedAntenatalTestingAndSurveillanceOrg] L'élément 'Coded Antenatal Testing and Surveillance Organizer' doit 
            au moins contenir une entrée 'Simple Observation' (1.3.6.1.4.1.19376.1.5.3.1.4.13)
        </assert> 
        

        <assert test="cda:id">
            [codedAntenatalTestingAndSurveillanceOrg] "Coded Antenatal Testing and Surveillance Organizer" aura nécessairement un identifiant &lt;id&gt;.
        </assert>

        <assert test="cda:statusCode[@code=&#34;completed&#34;]">
            [codedAntenatalTestingAndSurveillanceOrg] La valeur de l'élément "statusCode" de "Coded Antenatal Testing and Surveillance Organizer" est fixée à "completed".
        </assert>
        <assert test="cda:effectiveTime">
            [codedAntenatalTestingAndSurveillanceOrg] l'élément effectiveTime est requis. Il indique quand l'observation a été faite.
        </assert>

    </rule>
</pattern>
    <pattern id="comments-errors">
    
        <title>IHE PCC v3.0 Comments - errors validation phase</title>
        <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.2&#34;]">
            
            <assert test="cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.40&#34;]">
                Erreur de Conformité PCC: Le templateId CCD (2.16.840.1.113883.10.20.1.40) de l'entrée
                Comments doit être déclaré.</assert>
            
            <assert test="cda:code[@code=&#34;48767-8&#34; and                 @codeSystem=&#34;2.16.840.1.113883.6.1&#34;]">
                Erreur de Conformité PCC: L'élément "code" pour l'entrée "Comments" est requis. Ses attributs "code" et "codeSystem"
                sont obligatoires (cf. CI-SIS Volet de contenu CDA)</assert>
            
            <assert test="cda:statusCode[@code = &#34;completed&#34;]">
            Erreur de Conformité PCC: La valeur de l'élément "code" de "statusCode" est toujours fixée à "completed". </assert>
            
            <assert test="not(cda:author) or (                 cda:author/cda:time and                 cda:author/cda:assignedAuthor/cda:id and                 cda:author/cda:assignedAuthor/cda:addr and                 cda:author/cda:assignedAuthor/cda:telecom and                 cda:author/cda:assignedAuthor/cda:assignedPerson/cda:name and                 cda:author/cda:assignedAuthor/cda:representedOrganization/cda:name)">
                Erreur de Conformité PCC: Un élément "Comment" peut avoir un auteur.
                L'horodatage de la création de l'élément "Comment" est réalisé à partir de l'élément "time" lorsque l'élément "author" est présent.
                L'identifiant de l'auteur (id), son adresse (addr) et son numéro de téléphone (telecom) sont dans ce cas obligatoires. 
                Le nom de l'auteur et/ou celui de l'organisation qu'il représente doit être présent.</assert>
            
            
        </rule>
</pattern>
    <pattern id="concernEntry-errors">
    <title>IHE PCC v3.0 Concern Entry</title>
        <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.5.1&#34;]">
            <assert test="../cda:act">
                [concernEntry]  L'entrée "Concern Entry" ne peut être utilisée que comme un élément "act".</assert>
            
            <assert test="../cda:act[@classCode=&#34;ACT&#34;] and ../cda:act[@moodCode=&#34;EVN&#34;]">
                [concernEntry] une entrée "Concern Entry" est l'acte ("act classCode='ACT'") qui consiste 
                à enregistrer un événement (moodCode='EVN') relatif à un problème, une allergie ou tout autre élément se rapportant
                à l'état clinique d'un patient.</assert>
            
            <assert test="cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.27&#34;]">
                [concernEntry] Ces élément templateId indiquent que l'entrée "Concern Entry" se conforme 
                au module de contenu Concern. Celui-ci hérite des contraintes du template HL7 CCD pour les "problem acts", 
                et déclarera sa conformité à patir du templateId 2.16.840.1.113883.10.20.1.27.</assert>
            
            <assert test="cda:id">
                [concernEntry] L'entrée "Concern Entry" requiert un élément "id".</assert>
            
            <assert test="cda:code[@nullFlavor=&#34;NA&#34;]">
                [concernEntry] l'élément "code" n'est pas applicable à un élément "Concern Entry", et prendra la valeur nullFlavor='NA'.</assert>
            
            <assert test="cda:statusCode[@code=&#34;active&#34; or                  @code=&#34;suspended&#34; or                 @code=&#34;aborted&#34; or                 @code=&#34;completed&#34;]">
                [concernEntry] L'élément "statusCode" associé à tout élément concern doit prendre l'une des valeurs suivantes: 
                "active", "suspended", "aborted" ou "completed".</assert>
            
            <assert test="(cda:effectiveTime[@nullFlavor])or(cda:effectiveTime/cda:low)">
                [concernEntry] l'élément "effectiveTime" indique le début et la fin de la période durant laquelle l'élément "Concern Entry" était actif. 
                Son composant "low" ou un élément nullFlavor sera au moins présent.</assert>
            
            <assert test="(cda:effectiveTime[@nullFlavor]) or ((cda:statusCode[@code=&#34;completed&#34; or @code=&#34;aborted&#34;] and cda:effectiveTime/cda:high) or                 (cda:statusCode[@code=&#34;active&#34; or @code=&#34;suspended&#34;] and not(cda:effectiveTime/cda:high)))">
                [concernEntry] l'élément "effectiveTime" indiquele début et la fin de la période durant laquelle l'élément 
                "Concern Entry" était actif. 
                Son composant "high" (ou un élément nullFlavor ) sera présent pour les es éléments "Concern entry" ayant un statut "completed" ou "aborted" 
                et sera absent dans tous les autres cas</assert>
            
            <assert test="(cda:entryRelationship[@typeCode=&#34;SUBJ&#34;] and cda:entryRelationship/*/cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.5&#34; or @root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.6&#34;]) or                   (cda:sourceOf[@typeCode=&#34;SUBJ&#34; and @inversionInd=&#34;false&#34;] and cda:sourceOf/*/cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.5&#34; or @root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.6&#34;]) ">
                [concernEntry] Tout élément "Concern Entry" concerne un ou plusieurs problèmes ou allergies. 
                Cette entrée contient une ou plusieurs entrées qui se conforment aux spécifications de "Problem Entry" ou "Allergies and Intolerance Entry" 
                permettant à une série d'observations d'être regroupées en un unique élément "Concern Entry", ce à partir de liens de type entryRelationship 
                d'attribut typeCode='SUBJ' et inversionInd='false'</assert>
        </rule>
    </pattern>
    <pattern id="encountersEntry-errors">


    <title>IHE PCC v3.0 Encounters - errors validation phase</title>
    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.14']">

        <assert test="@classCode='ENC'"> 
            Erreur de Conformité PCC: Dans une entrée "Encounters", l'attribut "classCode" sera fixé à la valeur "ENC". </assert>

        <assert test="not(@moodCode='EVN') or cda:templateId[@root='2.16.840.1.113883.10.20.1.21']"> 
            Erreur de Conformité PCC: Dans une entrée "Encounters", le templateId indique que cet élément 
            se conforme aux contraintes de ce module de contenu.
            NOTE: Lorsque l'entrée "Encounters",est en mode événement, (moodCode='EVN'), cette entrée 
            se conforme au template CCD 2.16.840.1.113883.10.20.1.21, et dans les autres modes, 
            elle se conformera au template CCD 2.16.840.1.113883.10.20.1.25. </assert>
        
        <assert test="@moodCode='EVN' or cda:templateId[@root='2.16.840.1.113883.10.20.1.25']"> 
            Erreur de Conformité PCC: Dans une entrée "Encounters", le templateId indique que cet élément 
            se conforme aux contraintes de ce module de contenu.
            NOTE: Lorsque l'entrée "Encounters",est en mode événement, (moodCode='EVN'), cette entrée 
            se conforme au template CCD 2.16.840.1.113883.10.20.1.21, et dans les autres modes, 
            elle se conformera au template CCD 2.16.840.1.113883.10.20.1.25. </assert>
        
        <assert test="cda:id"> 
        Erreur de Conformité PCC: Dans une entrée "Encounters", l'élément "id" est obligatoire. </assert>
        
          
        
        <assert test="cda:text/cda:reference"> 
            Erreur de Conformité PCC: Dans une entrée "Encounters", l'élément "text" contiendra
            une référence à la partie narrative décrivant l'événement. </assert>
        
        <assert test="not(@moodCode = 'EVN' or @moodCode = 'APT') or cda:effectiveTime"> 
            Erreur de Conformité PCC: Dans une entrée "Encounters", l'élément "effectiveTime" 
            horodate l'événement (en mode EVN), ou la date désirée pour la rencontre, en mode ARQ or APT.
            En mode EVN ou APT, l'élément "effectiveTime" sera présent. En mode ARQ, l'élément "effectiveTime" 
            pourra être présent, et dans le cas contraire, l'élément "priorityCode" sera présent, 
            pour indiquer qu'un rappel est nécessaire pour fixer la date de rendez-vous pour la rencontre. </assert>
        
        <assert test="@moodCode='ARQ' and (cda:effectiveTime or cda:priorityCode)"> 
            Erreur de Conformité PCC: Dans une entrée "Encounters", en mode ARQ mood, si l'élément "effectiveTime" est absent,
            alors l'élément "priorityCode" sera présent. </assert>
        
        <assert test="not(cda:participant[@typeCode='LOC']) or                  cda:participant[@typeCode='LOC']/cda:participantRole[@classCode='SDLOC']"> 
            Erreur de Conformité PCC: Dans une entrée "Encounters", un élément "participant" avec un attribut "typeCode" 
            LOC pourra être présent pour donner l'indication sur le lieu où la rencontre doit ou s'est tenue. 
            Cet élément aura un élément "participantRole" d'attribut classCode='SDLOC' décrivant la localisation du service. </assert>
        
        <assert test="not(cda:particpant[@typeCode='LOC']) or                 cda:participant[@typeCode='LOC']/cda:playingEntity/cda:name"> 
            Erreur de Conformité PCC: Dans une entrée "Encounters", un élément "participant" d'attribut "typeCode='LOC'" 
            désignera un élément "playingEntity" avec son nom. </assert>
        
        

        <assert test="not(@moodCode='ARQ') or cda:effectiveTime"> 
            Erreur de Conformité PCC (alerte): Dans une entrée "Encounters", en mode ARQ, 
            l'élément "effectiveTime" doit être présent. </assert>
        
        <assert test="not(@moodCode='EVN') or cda:performer"> 
            Erreur de Conformité PCC (alerte): Dans une entrée "Encounters", en mode EVN mood, au moisn
            un élément "performer" devrait être présentpour identifier la personne délivrant un service (soins, consultation...)
            durant la rencontre. Plus d'un élément "performer" pourront être présents. </assert>
        
        <assert test="not(cda:particpant[@typeCode='LOC']) or                 cda:participant[@typeCode='LOC']/cda:addr"> 
            Erreur de Conformité PCC (alerte): Dans une entrée "Encounters", un élément "addr" devrait être présent
            comme partie de l'élément "participant" d'attribut "typeCode='LOC'". </assert>
        
        <assert test="not(cda:particpant[@typeCode='LOC']) or                 cda:participant[@typeCode='LOC']/cda:telecom"> 
            Erreur de Conformité PCC (alerte): Dans une entrée "Encounters", un élément "telecom" devrait être présent
            comme partie de l'élément "participant" d'attribut "typeCode='LOC'". </assert>
    </rule>
</pattern>
    <pattern id="immunizationsEnt-errors">
    <title>IHE PCC v3.0 Immunizations Section</title>
    
    
    
    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.12']">
        
        <assert test="@negationInd=&#34;true&#34; or @negationInd=&#34;false&#34;">
            Erreur de Conformité PCC: 
            Une entrée 'Immunization' peut être le moyen de notifier qu'une vaccination spécifique n'a pas eu lieu, et pourquoi. 
            Dans ce cas, l'attribut negationInd prendra la valeur 'true' et dans tous les autres cas la valeur 'false'.
        </assert>

        
        <assert test="cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.24&#34;]">
            Erreur de Conformité PCC: Immunization doit notifier l'OID du template CCD parent (2.16.840.1.113883.10.20.1.24).
        </assert>
        
        <assert test="cda:id">
            Erreur de Conformité PCC: Une vaccination aura un identifiant (id).
        </assert>
        
        <assert test="cda:code[@code and @codeSystem]">
            Erreur de Conformité PCC: 
            Cet élément obligatoire indique que l'acte effectué est une vaccination. 
            L'élément act substance administration doit présenter un élément 'code' avec des attributs 'code' et 'codeSystem' obligatoirement présents.
            Si aucun système de codage est utilisé, on utilisera les valeurs code='IMMUNIZ' codeSystem='2.16.840.1.113883.5.4' codeSystemName='ActCode'
        </assert>
        
        <assert test="cda:statusCode[@code=&#34;completed&#34;]">
            Erreur de Conformité PCC: L'élément 'statusCode' prendra la valeur 'completed' pour toutes les vaccinations.
        </assert>
        
        <assert test="cda:effectiveTime[@value or @nullFlavor]">
            Erreur de Conformité PCC: 
            
            Dans Immunizations, l'élément 'effectiveTime' sera obligatoirement présent, indiquant l'horodatage de la vaccination.
            Si la date est inconnue, l'attribut nullFlavor sera utilisé.
        </assert>
        
        <assert test="cda:consumable//cda:manufacturedProduct//cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.7.2&#34;]">
            Erreur de Conformité PCC: 
            Dans 'Immunizations', l'élément 'consumable' sera présent, and contiendra une entrée 'manufacturedProduc' se conformant au 
            template 'Product Entry template' (1.3.6.1.4.1.19376.1.5.3.1.4.7.2).
        </assert>
        
        <assert test="not(cda:entryRelationship[@inversionInd=&#34;false&#34; and @typeCode=&#34;CAUS&#34;]) or             (cda:entryRelationship[@inversionInd=&#34;false&#34; and @typeCode=&#34;CAUS&#34;]//cda:observation/cda:id and             cda:entryRelationship[@inversionInd=&#34;false&#34; and @typeCode=&#34;CAUS&#34;]//cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.28&#34;] and             cda:entryRelationship[@inversionInd=&#34;false&#34; and @typeCode=&#34;CAUS&#34;]//cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.5&#34;] and             cda:entryRelationship[@inversionInd=&#34;false&#34; and @typeCode=&#34;CAUS&#34;]//cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.54&#34;])">   
            Erreur de Conformité PCC: 
            Dans 'Immunizations', un élément entryRelationship pourra être utilisé pour identifier d'éventuelles réactions adverses 
            causées par la vaccination.
            Dans ce cas l'identifiant (id) de l'observation est requis. 
            L'observation se conformara au template 'Problem Entry', ainsi qu'au template 'CCD Reaction'.

        </assert>
        
        <assert test="cda:entryRelationship/cda:observation/cda:code[@code=&#34;30973-2&#34; and @codeSystem=&#34;2.16.840.1.113883.6.1&#34;]">
            Erreur de Conformité PCC: 
            dans l'élément entryRelationship permettant d'assigner le rang de la vaccination 
            dans une série de vaccinations effectuée (1ère vaccination, deuxième, etc), l'élément 'code' sera présent et ses 
            attributs prendront les valeurs (code='30973-2' displayName='Dose Number' codeSystem='2.16.840.1.113883.6.1' codeSystemName='LOINC')
            Cet élément indique que l'observation concerne le rand de la vaccination. 
        </assert>

        <assert test="not(ancestor::*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.12&#34;]) or              cda:statusCode[@code=&#34;completed&#34;]">
            Erreur de Conformité PCC: 
            dans l'élément entryRelationship permettant d'assigner le rang de la vaccination 
            dans une série de vaccinations effectuée (1ère vaccination, deuxième, etc), l'élément 'statusCode', obligatoire, 
            prendra la valeur 'completed'.
        </assert>
        
        <assert test="cda:entryRelationship/cda:observation/cda:value[@value]">
            Erreur de Conformité PCC: 
            Dans une entrée 'Immunization', dans l'élément entryRelationship permettant d'assigner le rang de la vaccination 
            dans une série de vaccinations effectuée (1ère vaccination, deuxième, etc), l'élément 'value' sera présent et 
            indiquera le numéro de lot du vaccin.
        </assert>

    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="observationInterpretation">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="cda:observation/cda:interpretationCode">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 1)">
           [dansJeuDeValeurs] L'élément "<value-of select="'observation/interpretationCode'"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (1 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="'observation/interpretationCode'"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_observationInterpretation)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="'observation/interpretationCode'"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_observationInterpretation"/>.
        </assert>
    </rule>
</pattern>
    <pattern id="problemConcernEntry-errors">
    <title>IHE PCC v3.0 Problem Concern Entry - errors validation phase</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.5.2&#34;]">

        <assert test="cda:templateId[@root = &#34;1.3.6.1.4.1.19376.1.5.3.1.4.5.1&#34;]"> 
            [problemConcernEntry] Problem Concern Entry a un template OID 1.3.6.1.4.1.19376.1.5.3.1.4.5.2. 
            Elle spécialise Concern Entry et doit donc se conformer à ses spécifications 
            en déclarant son template OID qui est 1.3.6.1.4.1.19376.1.5.3.1.4.5.1. Ces éléments 
            sont requis.
        </assert>
        
        <assert test="cda:templateId[@root = &#34;2.16.840.1.113883.10.20.1.27&#34;]"> 
            [problemConcernEntry] Le template parent de Problem Concern est absent.
        </assert>

    </rule>
</pattern>
    <pattern id="procedureEntry-errors">

        <title>IHE PCC v3.0 Procedure Entry</title>
        <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.19&#34;]">
            
            <assert test="self::cda:procedure[@classCode=&#34;PROC&#34;]">
                [procedureEntry]: L'attribut "classCode" pour un élément "Procedure Entry" sera fixé à la valeur "PROC".</assert>
            
            <assert test="not(./@moodCode=&#34;EVN&#34;) or                 cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.29&#34;]">
                [procedureEntry]: Lorsque l'élément "Procedure Entry" est en mode événement (moodCode='EVN'), 
                cette entrée se conforme au template CCD 2.16.840.1.113883.10.20.1.29</assert>
            
            <assert test="not(./@moodCode=&#34;INT&#34;) or                 cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.25&#34;]">
                [procedureEntry]: Lorsque l'élément "Procedure Entry" est en mode intention (moodCode='INT'),
                cette entrée se conforme au template CCD 2.16.840.1.113883.10.20.1.25.</assert>
            
            <assert test="cda:id">
                [procedureEntry]: Un élément "Procedure Entry" comporte un identifiant "id".</assert>
            
            <assert test="cda:code">
            [procedureEntry]: Un élément "Procedure Entry" comporte un élément "code".</assert>
            
            
            <assert test="cda:statusCode[@code = &#34;completed&#34; or                 @code = &#34;active&#34; or                 @code = &#34;aborted&#34; or                 @code = &#34;cancelled&#34;]">
                [procedureEntry]: L'élément "statusCode" sera présent.
                Il prendra la valeur "completed" pour les procédures réalisées, ou "active" pour les procédures 
                toujours en cours. Il prendra la valeur "aborted" por les procédures ayant été stoppées avant la fin 
                et "cancelled" pour celles qui ont été annulées (avant d'avoir débuté).</assert>
            
            <assert test="not(./@moodCode=&#34;INT&#34;) or                  (cda:effectiveTime or cda:priorityCode)">
                [procedureEntry]: dans une entrée "Procedure Entry", l'élément "priorityCode" sera présent en mode "INT" 
                lorsque l'élément "effectiveTime" est omis.
                Il peut cependant exister dans d'autres modes, indiquant le degré de priorité de la procédure.</assert>
    
            <assert test="./@moodCode = &#34;INT&#34; or ./@moodCode = &#34;EVN&#34;">
                [procedureEntry] (Alerte): L'attribut "moodCode" d'une entrée "Procedure Entry" peut prendre la valeur "INT" 
                pour indiquer une procédure escomptée, ou "EVN" pour indiquer qu'elle a déjà été réalisée.</assert>
            
            <assert test="cda:code[@code]">
                [procedureEntry] (Alerte): une entrée "Procedure Entry" devrait comporter un code décrivant le type de la procédure.</assert>
            
            <assert test="cda:effectiveTime">
                [procedureEntry] (Alerte): l'élément "effectiveTime" devrait être présent dans une entrée "Procedure Entry"
                pour horodater la procédure (en mode "EVN") ou la date escomptée pour la procédure (en mode "INT").</assert>
            
            
        </rule>
</pattern>
    <pattern id="problemEntry-errors">
    <title>IHE PCC v3.0 Problem Entry</title>
    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']">

        <assert test="self::cda:observation[@classCode='OBS' and @moodCode='EVN']">
            [problemEntry]: Dans l'élément "Problem Entry", le format de base utilisé pour 
            représenter un problème utilise l'élément CDA 'observation' d'attribut classCode='OBS' pour
            signifier qu'il s'agit l'observation d'un problème, et moodCode='EVN', pour exprimer 
            que l'événement a déjà eu lieu. </assert>


        <assert test="cda:templateId[@root='2.16.840.1.113883.10.20.1.28']"> 
            [problemEntry]: Dans l'élément "Problem Entry", les éléments &lt;templateId&gt; 
            identifient l'entrée comme répondant aux spécifications de PCC et de CCD (2.16.840.1.113883.10.20.1.28). 
            Cette déclaration de conformité est requise.</assert>

        <assert test="count(./cda:id) = 1"> 
            [problemEntry]: L'élément "Problem Entry" doit nécessairement avoir un identifiant (&lt;id&gt;) 
            qui est utilisé à des fins de traçage. Si la source d'information du SIS ne fournit pas d'identifiant, 
            un GUID sera affecté comme attribut "root", sans extension (ex: id root='CE1215CD-69EC-4C7B-805F-569233C5E159'). 
            Bien que CDA permette l'utilisation de plusieurs identifiants, "Problem Entry" impose qu'un identifiant 
            seulement soit présent. </assert>

        <assert test="cda:statusCode[@code='completed']"> 
            [problemEntry]: Un élément "Problem Entry" décrit l'observation d'un fait clinique. 
            Son composant "statutCode" sera donc toujours fixé à la valeur code='completed'. </assert>

        <report test="cda:effectiveTime/cda:width or cda:effectiveTime/cda:center"> 
            [problemEntry]: Bien que CDA permette de nombreuses modalités pour exprimer un intervalle de 
            temps (low/high, low/width, high/width, ou center/width), Problem Entry sera contraint à l'utilisation
            exclusive de la forme low/high.</report>

        <assert test="cda:effectiveTime/cda:low[@value or @nullFlavor = 'UNK'] or cda:effectiveTime/cda:low[@value or @nullFlavor = 'NAV']"> 
            [problemEntry]: La composante "low" de l'élément "effectiveTime" doit être exprimée dans 
            un élément "Problem Entry".
            Des exceptions sont cependant admises, comme dans le cas où le patient ne se souvient pas de 
            la date de survenue d'une affection (ex: rougeole dans l'enfance sans date précise).
            Dans ce cas, l'élément "low" aura pour attribut un "nullFlavor" fixé à la valeur 'UNK'. </assert>

        <assert test="cda:value[@xsi:type='CD']"> 
            [problemEntry]: L'élément "value" correspond à l'état (clinique) décritet est donc obligatoire.
            Cet élément est toujours codé et son type sera toujours de type 'CD' (xsi:type='CD'). </assert>

        <assert test="(cda:value[@code and @codeSystem]) or                     (not(cda:value[@code]) and not(cda:value[@codeSystem]))"> 
            [problemEntry]: Si l'élément "value" est codé, les attributs "code" et "codeSystem" 
            seront obligatoirement présents. </assert>
        
        
        
        
        <assert test="count(cda:entryRelationship/cda:observation/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.1']) &lt;= 1"> 
            [problemEntry]: Un et un seul élément évaluant la sévérité d'une affection 
            sera présent (entryRelationship) pour une entrée "Problem Entry" </assert>

        <assert test="not(cda:entryRelationship/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.1']) or                     (cda:entryRelationship/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.1'] and                     cda:entryRelationship[@typeCode='SUBJ' and @inversionInd='true'])"> 
            [problemEntry]: un élément "entryRelationship" optionnel peut être présent 
            et donner une indication sur la sévérité d'une affection. S'il est présent, cet élément 
            se conformera au template Severity Entry (1.3.6.1.4.1.19376.1.5.3.1.4.1).
            Son attribut 'typeCode' prendra alors la valeur 'SUBJ' et 'inversionInd' la valeur 'true'. </assert>
        
        
        <assert test="count(cda:entryRelationship/cda:observation/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.1.1']) &lt;= 1"> 
            [problemEntry]: Un et un seul élément évaluant le statut d'une affection (Problem Status Observation)
            sera présent par le biais d'une relation "entryRelationship" pour toute entrée "Problem Entry"</assert>

        <assert test="not(cda:entryRelationship/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.1.1']) or                     (cda:entryRelationship/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.1.1'] and                     cda:entryRelationship[@typeCode='REFR' and @inversionInd='false'])"> 
            [problemEntry]: un élément "entryRelationship" optionnel peut être présent 
            et donner une indication sur le statut clinique d'une affection -- cf. value set "PCC_ClinicalStatusCodes" (1.2.250.1.213.1.1.4.2.283.2). 
            S'il est présent, cet élément se conformera au template "Problem Status Observation" (1.3.6.1.4.1.19376.1.5.3.1.4.1.1).
            Son attribut 'typeCode' prendra alors la valeur 'REFR' et 'inversionInd' la valeur 'false'.</assert>

        
        <assert test="count(cda:entryRelationship/cda:observation/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.1.2']) &lt;= 1"> 
            [problemEntry]: Un et un seul élément évaluant le statut de l'état de santé 
            d'un patient (Health Status Observation) sera présent par le biais d'une relation "entryRelationship" 
            pour toute entrée "Problem Entry". </assert>

        <assert test="not(cda:entryRelationship/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.1.2']) or                     (cda:entryRelationship/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.1.2'] and                     cda:entryRelationship[@typeCode='REFR' and @inversionInd='false'])"> 
            [problemEntry]: un élément "entryRelationship" optionnel peut être présent et donner
            une indication sur le statut de l'état de santé d'un patient -- cf. value set "PCC_HealthStatusCodes" (1.2.250.1.213.1.1.4.2.283.1). 
            S'il est présent, cet élément se conformera au template "Health Status Observation" (1.3.6.1.4.1.19376.1.5.3.1.4.1.2).
            Son attribut 'typeCode' prendra alors la valeur 'REFR' et 'inversionInd' la valeur 'false'.</assert>
        
        
        <assert test="not(cda:entryRelationship/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.2']) or                     (cda:entryRelationship/cda:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.2'] and                     cda:entryRelationship[@typeCode='SUBJ' and @inversionInd='true'])"> 
            [problemEntry]: un ou plusieurs éléments "entryRelationship" optionnels peuvent être présents et 
            permettre d'apporter des informations additionnelles sur le problème observé.
            S'il est présent, cet élément se conformera au template "Comment Entry" (1.3.6.1.4.1.19376.1.5.3.1.4.2).
            Son attribut 'typeCode' prendra alors la valeur 'SUBJ' et 'inversionInd' la valeur 'true'.</assert>

        <assert test="cda:code">  
            [problemEntry] (Alerte): L'élément code -- cf. jeu de valeurs "PCC_ProblemCodes" (1.2.250.1.213.1.1.4.2.283.3) 
            d'une entrée Problem Entry permet d'établir à quel stade diagnostique se positionne un problème : par exemple un diagnostic 
            est un stade plus évolué qu'un symptôme dans la description d'un problème. Cette évaluation est importante pour les cliniciens. </assert>
        
        
        <report test="cda:uncertaintyCode"> 
            [problemEntry] (Alerte): CDA permet à la description d'un état clinique un certain degré d'incertitude avec 
            l'élément "uncertaintyCode". En l'absence actuelle de consensus clairement établi sur le bon usage de cet élément, 
            PCC déconseille de l'utiliser dans le cadre d'une entrée Problem Entry.</report>   
        
        <report test="cda:confidentialityCode"> 
            [problemEntry] (Alerte): CDA permet l'utilisation de l'élément "confidentialtyCode" pour une observation.
            PCC déconseille cependant pour des raisons pratiques de l'utiliser dans le cadre d'une entrée Problem Entry.
            Il y a en effet d'autres manières d'assurer la confidentialité des documents, qui pourront être résolus au sein
            du domaine d'affinité.</report>
        
        <assert test="not(cda:value[@codeSystem]) or cda:value[@codeSystemName]"> 
            [problemEntry] (Alerte): les attributs "codeSystem" et "codeSystemName" de l'élément "value" d'une 
            entrée Problem Entry devraient être présents pour une meilleure lisibilité, mais ne sont pas obligatoires. </assert>
        
        <assert test="not(cda:value[@code]) or cda:value[@displayName]"> 
            [problemEntry] (Alerte): l'attribut "displaySystemName" de l'élément "value" d'une 
            entrée Problem Entry devrait être présent pour une meilleure lisibilité, mais n'est pas obligatoire.</assert>
    </rule>
</pattern>
    <pattern id="simpleObservation-errors">
    <title>IHE PCC v3.0 Simple Observation</title>
    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']">
        
        <assert test="cda:id">
            Erreur de Conformité PCC: "Simple Observation" requiert un élément identifiant &lt;id&gt;.</assert>
        
        <assert test="cda:code">
            Erreur de Conformité PCC: "Simple Observation" requiert un élément "code" décrivant ce qui est observé.</assert>
        
        <assert test="cda:statusCode[@code = &#34;completed&#34;]">
            Erreur de Conformité PCC: L'élément "statusCode" est requis dans "Simple Observations" 
            sont fixés à la valeur "completed".</assert>
        
        <assert test="cda:effectiveTime[@value or @nullFlavor] or cda:effectiveTime/cda:low[@value or @nullFlavor]">
            Erreur de Conformité PCC: L'élément &lt;effectiveTime&gt; est requis dans "Simple Observations",
            et représentera la date et l'heure de la mesure effectuée. Cet élément devrait être précis au jour. 
            Si la date et l'heure sont inconnues, l'attribut nullFlavor sera utilisé.</assert>
        
        <assert test="cda:value">
            Erreur de Conformité PCC: L'élément "value" d'un élément "Simple Observation" utilisera un 
            type de donnée approprié à l'observation.</assert>        
        
    </rule>
    
    
</pattern>  

    
                                 
    

    <pattern id="CSECodedVitalSignsEnt-errors">
    <title>IHE PCC v3.0 Vital Signs Observation</title>
    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.2']">
        
        <assert test="cda:templateId[@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.13&#34;] and             cda:templateId[@root=&#34;2.16.840.1.113883.10.20.1.31&#34;]">
            Erreur de Conformité volet CSE: Les OID des templates parents 
            (2.16.840.1.113883.10.20.1.31 et 1.3.6.1.4.1.19376.1.5.3.1.4.13) 
            de l'élément 'Vital Signs Observation' doivent être présents.
        </assert>
        <assert test="cda:code[@codeSystem = &#34;2.16.840.1.113883.6.1&#34;]">
            Erreur de Conformité volet CSE: L'attribut 'codeSystem' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1)
        </assert>
        <assert test="cda:code[@code = &#34;9279-1&#34; or              @code = &#34;8867-4&#34; or             @code = &#34;2710-2&#34; or             @code = &#34;8480-6&#34; or             @code = &#34;8462-4&#34; or             @code = &#34;8310-5&#34; or             @code = &#34;8302-2&#34; or             @code = &#34;8306-3&#34; or             @code = &#34;8287-5&#34; or             @code = &#34;3141-9&#34; or             @code = &#34;41909-3&#34; or             @code = &#34;9272-6&#34; or             @code = &#34;9274-2&#34;]">
            Erreur de Conformité volet CSE: L'attribut 'code' doit être codé dans la nomenclature LOINC selon les valeurs prévues. 
        </assert>
        <assert test="cda:value[@xsi:type=&#34;PQ&#34; or @xsi:type=&#34;REAL&#34; or @xsi:type=&#34;INT&#34;] ">
            Erreur de Conformité volet CSE: L'attribut 'value' sera présent et s'exprimera dans le type de donnée spécifiée par le volet.
        </assert>
        
        <assert test="not(cda:code[@code=&#34;9279-1&#34;]) or cda:value[@unit=&#34;/min&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 9279-1 
            aura une valeur s'exprimant en "/min".           
        </assert>
        <assert test="not(cda:code[@code=&#34;8867-4&#34;]) or cda:value[@unit=&#34;/min&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 8867-4 
            aura une valeur s'exprimant en "/min".
        </assert>
        <assert test="not(cda:code[@code=&#34;2710-2&#34;]) or cda:value[@unit=&#34;%&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 2710-2  
            aura une valeur s'exprimant en "%".
        </assert>
        <assert test="not(cda:code[@code=&#34;8480-6&#34;]) or cda:value[@unit=&#34;mm[Hg]&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 8480-6
            aura une valeur s'exprimant en "mm[Hg]".
        </assert>
        <assert test="not(cda:code[@code=&#34;8462-4&#34;]) or cda:value[@unit=&#34;mm[Hg]&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 8462-4
            aura une valeur s'exprimant en "mm[Hg]".
        </assert>
        <assert test="not(cda:code[@code=&#34;8310-5&#34;]) or cda:value[@unit=&#34;Cel&#34; or @unit=&#34;[degF]&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 8310-5
            aura une valeur s'exprimant en "Cel" or "[degF]".
        </assert>
        <assert test="not(cda:code[@code=&#34;8302-2&#34;]) or cda:value[@unit=&#34;m&#34; or             @unit=&#34;cm&#34; or @unit=&#34;[in_us]&#34; or @unit=&#34;[in_uk]&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 8302-2
            aura une valeur s'exprimant en "m" ou "cm" ou "[in_us]" ou "[in_uk]".
        </assert>
        <assert test="not(cda:code[@code=&#34;8306-3&#34;]) or cda:value[@unit=&#34;m&#34; or             @unit=&#34;cm&#34; or @unit=&#34;[in_us]&#34; or @unit=&#34;[in_uk]&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 8306-3
            aura une valeur s'exprimant en "m" ou "cm" ou "[in_us]" ou "[in_uk]".
        </assert>
        <assert test="not(cda:code[@code=&#34;8287-5&#34;]) or cda:value[@unit=&#34;m&#34; or             @unit=&#34;cm&#34; or @unit=&#34;[in_us]&#34; or @unit=&#34;[in_uk]&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 8287-5
            aura une valeur s'exprimant en "m" ou "cm" ou "[in_us]" ou "[in_uk]".
        </assert>
        <assert test="not(cda:code[@code=&#34;3141-9&#34;]) or cda:value[@unit=&#34;kg&#34; or             @unit=&#34;g&#34; or @unit=&#34;[lb_av]&#34; or @unit=&#34;[oz_av]&#34;]">
            Erreur de Conformité volet CSE: Un élément 'Vital Signs Observation' codé 3141-9
            aura une valeur s'exprimant en "kg" ou "g" ou "[lb_av]" ou "[oz_av]".
        </assert>

    </rule>
</pattern>
    <pattern id="CSECodedAntenatalTestingAndSurveillanceEnt-errors">
    <title>IHE PCC v3.0 Coded Antenatal Testing And Surveillance</title>
    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.3.10']">
        
        <assert test="cda:component/cda:observation/cda:code[@code = &#34;XX-MCH031&#34;]">
            Erreur de Conformité volet CSE: La présence ou l'absence d'échographie morphologique doit être signalée. 
        </assert>
        
        <assert test="cda:component/cda:observation/cda:code[@code = &#34;CSE-049&#34;]">
            Erreur de Conformité volet CSE: La présence ou l'absence de mesure de la clarté nucale  doit être signalée. 
        </assert>
        
        <assert test="cda:component/cda:observation/cda:code[@code = &#34;XX-MCH032&#34;]">
            Erreur de Conformité volet CSE: Le Nombre total d'échographies réalisé doit être signalé. 
        </assert>
        
        <assert test="cda:component/cda:observation[cda:code/@code = &#34;XX-MCH032&#34;]/cda:value[@xsi:type=&#34;INT&#34;] ">
            Erreur de Conformité volet CSE: L'attribut 'value' sera présent et s'exprimera dans le type de donnée spécifiée par le volet.
        </assert>
        
        <assert test="cda:component/cda:observation[cda:code/@code = &#34;5196-1&#34;]/@negationInd ">
            Erreur de Conformité volet CSE: L'attribut 'negationInd' de l'observation sera présent et exprimera que l'examen aura été pratiqué.
        </assert>
        
        <assert test="cda:component/cda:observation/cda:code[@code = &#34;5196-1&#34;]">
            Erreur de Conformité volet CSE: La présence ou l'absence de recherche d'Ag HBs et son résultat doivent être signalés
        </assert>
    </rule>
</pattern>
    <pattern id="CSEcodedResultsEntries-errors">

    
    
    <title>IHE PCC Coded Results Section</title>
    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.28']">
        
        
        
        <assert test="cda:entry/cda:procedure/cda:code[@code ='XX-MCH034']">
            Erreur de Conformité volet CS8: L'absence ou la présence d'échographie Morphologique doit être indiquée
        </assert>
        
        <assert test="cda:entry/cda:procedure/cda:code[@code ='P5-B005F']">
            Erreur de Conformité volet CS8: L'absence ou la présence de mesure de la clarté nucale doit être indiquée
        </assert>
        
        
        <assert test="cda:entry/cda:observation/cda:code[@code ='XX-MCH032']">
            Erreur de Conformité volet CS8: Le nombre total d'échographies doit être indiqué
        </assert>
        
        <assert test="cda:entry/cda:observation/cda:code[@code ='5196-1']">
            Erreur de Conformité volet CS8: La recherche d'antigène HBs doit être indiquée
        </assert>
        
        <assert test="cda:entry/cda:observation[cda:code/@code ='5196-1']/cda:subject/cda:relatedSubject/cda:code/@code='MTH'">
            Erreur de Conformité volet CS8: La recherche d'antigène HBs doit être attribuée à la mère par une entité "subject"
        </assert>       

    </rule>
</pattern>
    <pattern id="CSEcodedSocialHistoryEnt-errors">
    <title>IHE PCC v3.0 Coded Social History Section</title>
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.16.1&#34;]">
        
        
        <assert test="//cda:entry/cda:observation[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.13.4&#34;]/cda:code/@code=&#34;S-00610&#34;">             
            [CSEcodedSocialHistoryEnt] : L'entrée "niveau de scolarité" doit obligatoirement faire partie des entrées de la section "Coded Social History"
        </assert>
        <assert test=".//cda:entry/cda:observation[cda:code/@code=&#34;S-00610&#34;]/cda:subject[@typeCode=&#34;SBJ&#34;]/cda:relatedSubject/cda:code/@code=&#34;MTH&#34;">             
            [CSEcodedSocialHistoryEnt] : l'entrée "niveau de scolarité" doit obligatoirement être affectée à la mère
        </assert>
      
        
        <assert test=".//cda:entry/cda:observation[cda:subject/cda:relatedSubject/cda:code/@code=&#34;MTH&#34;]/cda:code/@code=&#34;J-00000&#34;">             
            [CSEcodedSocialHistoryEnt] : L'entrée "Profession" doit obligatoirement faire partie des entrées de la section 
            "Coded Social History attribuées à la mère de l'enfant"
        </assert>
        
        <assert test=".//cda:entry/cda:observation[cda:subject/cda:relatedSubject/cda:code/@code=&#34;FTH&#34;]/cda:code/@code=&#34;J-00000&#34;">             
            [CSEcodedSocialHistoryEnt] : L'entrée "Profession" doit obligatoirement faire partie des entrées de la section 
            "Coded Social History attribuées au père de l'enfant"
        </assert>        
        
        <assert test=".//cda:entry/cda:observation[cda:subject/cda:relatedSubject/cda:code/@code=&#34;MTH&#34;]/cda:code/@code=&#34;CSE-039&#34;">             
            [CSEcodedSocialHistoryEnt] : L'entrée "Statut professionnel" doit obligatoirement faire partie des entrées de la section 
            "Coded Social History attribuées à la mère de l'enfant"
        </assert>
        
        <assert test=".//cda:entry/cda:observation[cda:subject/cda:relatedSubject/cda:code/@code=&#34;FTH&#34;]/cda:code/@code=&#34;CSE-039&#34;">             
            [CSEcodedSocialHistoryEnt] : L'entrée "Statut professionnel" doit obligatoirement faire partie des entrées de la section 
            "Coded Social History attribuées au père de l'enfant"
        </assert>
        
        <assert test=".//cda:entry/cda:observation[cda:subject/cda:relatedSubject/cda:code/@code=&#34;MTH&#34;]/cda:code/@code=&#34;CSE-039&#34;">             
            [CSEcodedSocialHistoryEnt] : L'entrée "Statut professionnel" doit obligatoirement faire partie des entrées de la section 
            "Coded Social History attribuées à la mère de l'enfant"
        </assert>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;]) or             (.//cda:entry/cda:observation/cda:code/@code=&#34;CSE-041&#34;)">             
            [CSEcodedSocialHistoryEnt] : L'entrée "Type de garde de l'enfant" doit obligatoirement faire partie des entrées de la section 
            "Coded Social History.
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation/cda:code/@code=&#34;CSE-042&#34;">             
            [CSEcodedSocialHistoryEnt] : L'entrée "Garde à temps complet" doit obligatoirement faire partie des entrées de la section 
            "Coded Social History.
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation/cda:code/@code=&#34;CSE-043&#34;">             
            [CSEcodedSocialHistoryEnt] : L'entrée "Risque de staurnisme" doit obligatoirement faire partie des entrées de la section 
            "Coded Social History.
        </assert>
    </rule>
</pattern>
    
    <pattern id="CSEearsEnt-errors">
    <title>IHE PCC v3.0 Ears</title>
    
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.21&#34;]">
        
        
        <assert test=".//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;P2-A7100&#34;">
            [CSEearsEnt] L'absence ou la présence d'un examen auditif normal doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
    </rule>
</pattern>
    <pattern id="CSEgeneralAppEnt-errors">
    <title>IHE PCC v3.0 General appearance</title>
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.16&#34;]">       
        
        
        <assert test=".//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-02214&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence de trisomie 21 doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        
        <assert test=".//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-00000&#34;">
        Erreur de Conformité volet CSE: L'absence ou la présence d'une autre anomalie congénitale 
        doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
    <report test="(.//cda:entry/cda:observation[@negationInd=&#34;false&#34;]/cda:value/@code=&#34;D4-00000&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;D4-00000&#34;]/cda:entryRelationship/cda:act/cda:text=&#34;&#34;)">
        Erreur de Conformité volet CSE: Dans le cas d'une autre anomalie congénitale, préciser laquelle 
    </report>
        
    <report test="(.//cda:entry/cda:observation[@negationInd=&#34;true&#34;]/cda:value/@code=&#34;D4-00000&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;D4-00000&#34;]/cda:entryRelationship/cda:act/cda:text!=&#34;&#34;)"> 
            Erreur de Conformité volet CSE: Dans le cas d'une autre affection du système signalée, 
            le booléen cda:observation/@negationInd doit être fixé à la valeur "false" 
            sinon l'élément cda:entry/cda:observation/cda:entryRelationship/cda:act/cda:text doit être vide
        </report>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;XX-MCH207&#34;">
        Erreur de Conformité volet CSE: L'absence ou la présence d'une autre pathologie doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        <report test="(.//cda:entry/cda:observation[@negationInd=&#34;false&#34;]/cda:value/@code=&#34;XX-MCH207&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;XX-MCH207&#34;]/cda:entryRelationship/cda:act/cda:text=&#34;&#34;)">
        Erreur: Dans le cas d'une autre pathologie, préciser laquelle 
        </report>
        <report test="(.//cda:entry/cda:observation[@negationInd=&#34;true&#34;]/cda:value/@code=&#34;XX-MCH207&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;XX-MCH207&#34;]/cda:entryRelationship/cda:act/cda:text!=&#34;&#34;)"> 
            Erreur de Conformité volet CSE: Dans le cas d'une autre affection du système signalée, 
            le booléen cda:observation/@negationInd doit être fixé à la valeur "false" 
            sinon l'élément cda:entry/cda:observation/cda:entryRelationship/cda:act/cda:text doit être vide
        </report>
    </rule>
</pattern>
    <pattern id="CSEGenitaliaEnt-errors">
    <title>IHE PCC v3.0 Genitalia</title>
    
    

        <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.36&#34;]">
            
            
            <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or                 .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-80000&#34;">
                Erreur de Conformité volet CSE: L'absence ou la présence malformation génitale doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
            </assert>
            
            <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or                 .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-70000&#34;">
                Erreur de Conformité volet CSE: L'absence ou la présence de malformation urinaire doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
            </assert>
            
            <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.1&#34;]) or                 .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-71020&#34;">
                Erreur de Conformité volet CSE: L'absence ou la présence de malformation rénale doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
            </assert>
            
            <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or                 .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D7-00000&#34;">
                Erreur de Conformité volet CSE: L'absence ou la présence d'une autre affection du système urogénital
                doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
            </assert>
            
            <report test="(.//cda:entry/cda:observation[@negationInd=&#34;false&#34;]/cda:value/@code=&#34;D7-00000&#34;) and                  (.//cda:entry/cda:observation[cda:value/@code=&#34;D7-00000&#34;]/cda:entryRelationship/cda:act/cda:text=&#34;&#34;)">
                Erreur: Dans le cas d'une autre pathologie, préciser laquelle 
            </report>
            <report test="(.//cda:entry/cda:observation[@negationInd=&#34;true&#34;]/cda:value/@code=&#34;D7-00000&#34;) and                  (.//cda:entry/cda:observation[cda:value/@code=&#34;D7-00000&#34;]/cda:entryRelationship/cda:act/cda:text!=&#34;&#34;)"> 
                Erreur de Conformité volet CSE: Dans le cas d'une autre affection du système signalée, 
                le booléen cda:observation/@negationInd doit être fixé à la valeur "false" 
                sinon l'élément cda:entry/cda:observation/cda:entryRelationship/cda:act/cda:text doit être vide
            </report>
        </rule>

</pattern>
    <pattern id="CSEHeartEnt-errors">
    <title>IHE PCC v3.0 Heart</title>
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.29&#34;]">

        
        <assert test=".//cda:observation[@negationInd]/cda:value/@code=&#34;D4-31000&#34;"> 
            
            Erreur de Conformité volet CSE: L'absence ou la présence de Malformation cardiaque doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D3-00000&#34;"> 
            Erreur de Conformité volet CSE: L'absence ou la présence d'une autre affection du système cardiovasculaire
            doit obligatoirement être mentionnée à partir du booléen "observation/@negationInd"
        </assert>
        <report test="(.//cda:observation[@negationInd=&#34;false&#34;]/cda:value/@code=&#34;D3-00000&#34;) and              (.//cda:observation[cda:value/@code=&#34;D3-00000&#34;]/cda:entryRelationship/cda:act/cda:text=&#34;&#34;)"> 
            
            Erreur de Conformité volet CSE: Dans le cas de la présence d'autres affections du système, préciser lesquelles 
        </report>        
        <report test="(.//cda:observation[@negationInd=&#34;true&#34;]/cda:value/@code=&#34;D3-00000&#34;) and              (.//cda:observation[cda:value/@code=&#34;D3-00000&#34;]/cda:entryRelationship/cda:act/cda:text!=&#34;&#34;)"> 
            Erreur de Conformité volet CSE: Dans le cas d'une autre affection du système signalée, 
            le booléen cda:observation/@negationInd doit être fixé à la valeur "false" 
            sinon l'élément cda:entry/cda:observation/cda:entryRelationship/cda:act/cda:text doit être vide
        </report>
    </rule>
</pattern>
     <pattern id="CSENeurogicEnt-errors">
    <title>IHE PCC v3.0 Neurologic System</title>
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.35&#34;]">

                
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-95100&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence de Spina Bifida doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert> 
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;DA-26510&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence d'Infirmité motrice cérébrale doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.1&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;CSE-060&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence d'anomalie du tube neural doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.1&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-91300&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence d'hydrocéphalie doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>

        
        <assert test=".//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-51450&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence de Fente labio-palatine doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>       
 
        
        <assert test=".//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-0100&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence de Syndrome polymalformatif doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-90000&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence d'une autre affection du système nerveux
            doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        <report test="(.//cda:entry/cda:observation[@negationInd=&#34;false&#34;]/cda:value/@code=&#34;D4-90000&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;D4-90000&#34;]/cda:entryRelationship/cda:act/cda:text=&#34;&#34;)">
            Erreur de Conformité volet CSE: Dans le cas d'une autre pathologie, préciser laquelle 
        </report>
        <report test="(.//cda:entry/cda:observation[@negationInd=&#34;true&#34;]/cda:value/@code=&#34;D4-90000&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;D4-90000&#34;]/cda:entryRelationship/cda:act/cda:text!=&#34;&#34;)"> 
            Alerte: Dans le cas d'une autre affection du système signalée, 
            le booléen cda:observation/@negationInd doit être fixé à la valeur "false" 
            sinon l'élément cda:entry/cda:observation/cda:entryRelationship/cda:act/cda:text doit être vide
        </report>
    </rule>
    
</pattern>
    <pattern id="CSEabdomenEnt-errors">
    <title>IHE PCC v3.0 Abdomen</title>
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.31&#34;]">
        
                                        
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.1&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-55002&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence d'atrésie de l'oesophage doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.1&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;XX-MCH147&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence d'omphalocèle doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>

        
                                    
               
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D5-30140&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence de reflux gastro-oesophagien doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;R-21072&#34;">
            Erreur: L'absence ou la présence d'une autre affection du système digestif
            doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
                                             
        
        
        <report test="(.//cda:entry/cda:observation[@negationInd=&#34;false&#34;]/cda:value/@code=&#34;R-21072&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;R-21072&#34;]/cda:entryRelationship/cda:act/cda:text=&#34;&#34;)">
            Erreur de Conformité volet CSE: Dans le cas d'une autre pathologie du système digestif, préciser laquelle 
        </report>
        
        <report test="(.//cda:entry/cda:observation[@negationInd=&#34;true&#34;]/cda:value/@code=&#34;R-21072&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;R-21072&#34;]/cda:entryRelationship/cda:act/cda:text!=&#34;&#34;)"> 
            Erreur de Conformité volet CSE: Dans le cas d'une autre affection du système signalée, 
            le booléen cda:observation/@negationInd doit être fixé à la valeur "false" 
            sinon l'élément cda:entry/cda:observation/cda:entryRelationship/cda:act/cda:text doit être vide
        </report>
    </rule>
</pattern>
    <pattern id="CSEimmunizationsEnt-errors">
    <title>IHE PCC v3.0 Immunization Section</title>
    
    
    
    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.12']">
        <assert test="cda:consumable/cda:manufacturedProduct/cda:manufacturedMaterial/cda:code[@code = &#34;CSE-055&#34; or              @code = &#34;P2-47026&#34; or             @code = &#34;P2-47020&#34; or             @code = &#34;P2-47430&#34; or             @code = &#34;P2-47120&#34; or             @code = &#34;P2-47044&#34; or             @code = &#34;P2-47050&#34; or             @code = &#34;XX-MCH193&#34;]">
            [CSEimmunizationsEnt] : L'attribut 'code' doit être codé selon les valeurs prévues. 
        </assert>
    </rule>
</pattern>
    <pattern id="CSElaborAndDeliveryEnt-errors">
    <title>CSE Labor and Delivery entries</title>

    <rule context="*[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']">
        
        <assert test="cda:subject/cda:relatedSubject/cda:code/@code='MTH'"> 
        Erreur de conformité au volet CS8: La section "Labor and Delivery" dans le contexte du CS8 doit être attribué à la mère.</assert>
        
        <assert test="cda:component/cda:section[cda:templateId/@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.7']/cda:entry/cda:observation/cda:code/@code='F-84640'"> 
        Erreur de conformité au volet CS8: Cette section doit mentionner le type du début du travail (F-84640).</assert>
        
        <assert test="cda:component/cda:section[cda:templateId/@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.7']/cda:entry/cda:observation/cda:code/@code='CSE-029'"> 
            Erreur de conformité au volet CS8: Cette section doit mentionner l'analgésie utilisée au cours de l'accouchement(CSE-029).</assert>
        
        <assert test="cda:component/cda:section[cda:templateId/@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.7']/cda:entry/cda:observation/cda:code/@code='XX-MCH219'"> 
            Erreur de conformité au voletCS8: Cette section doit mentionner le type d'accouchement réalisé (XX-MCH219).</assert>
    </rule>
</pattern>
    <pattern id="CSERechHbs-errors">
    <title>IHE PCC v3.0 Ears</title>
    
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.21.3.10&#34;]">
        <assert test=".//cda:observation[cda:code/@code=&#34;5196-1&#34;]/cda:value[@code=&#34;CSE-075&#34; or @code=&#34;CSE-074&#34; or @nullFlavor]">
            [CSERechHbs] La recherche d'Ag Hbs doit être négative, positive, ou non faite. Dans ce dernier cas, l'élément nullFlavor doit être utilisé
        </assert>
    </rule>
</pattern>
    <pattern id="CSECatProf-errors">
    <title>IHE PCC v3.0 Ears</title>
    
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.16.1&#34;]">
        <assert test=".//cda:observation[cda:code/@code=&#34;J-00000&#34; and (cda:subject/cda:relatedSubject/cda:code/@code=&#34;MTH&#34;)] /cda:value[             @code=&#34;1&#34; or              @code=&#34;2&#34; or              @code=&#34;3&#34; or             @code=&#34;4&#34; or             @code=&#34;5&#34; or             @code=&#34;6&#34; or             @nullFlavor]">
            [CSECatProf] Le codage de la CSP doit faire partie des valeurs prévues dans le jeu de valeurs CSE_Prof
            ou utiliser l'élément nullFlavor.
        </assert>
        <assert test=".//cda:observation[cda:code/@code=&#34;J-00000&#34; and (cda:subject/cda:relatedSubject/cda:code/@code=&#34;FTH&#34;)] /cda:value[             @code=&#34;1&#34; or              @code=&#34;2&#34; or              @code=&#34;3&#34; or             @code=&#34;4&#34; or             @code=&#34;5&#34; or             @code=&#34;6&#34; or             @nullFlavor]">
            [CSECatProf] Le codage de la CSP doit faire partie des valeurs prévues dans le jeu de valeurs CSE_Prof
            ou utiliser l'élément nullFlavor.
        </assert>
    </rule>
</pattern>
    
    <pattern id="CSEpregnancyHistoryEntries-errors">
    <title>IHE PCC v3.0 Pregnancy History Section</title>
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.4.13.5&#34;]"> 
 
        <assert test="cda:code[@code=&#34;XX-MCH027&#34; or                         @code=&#34;11977-6&#34; or             @code=&#34;11640-0&#34; or             @code=&#34;11637-6&#34; or             @code=&#34;CSE-045&#34; or             @code=&#34;CSE-046&#34; or             @code=&#34;XX-MCH019&#34; or             @code=&#34;11878-6&#34; or             @code=&#34;11884-4&#34; or              @code=&#34;D8-20432&#34; or             @code=&#34;P2-87524&#34; or             @code=&#34;XX-MCH029&#34; or             @code=&#34;XX-MCH035&#34; or             @code=&#34;CSE-047&#34; or             @code=&#34;D8-11210&#34; or             @code=&#34;D8-11120&#34; or             @code=&#34;DB-61400&#34; or             @code=&#34;CSE-048&#34; or             @code=&#34;XX-MCH204&#34; or             @code=&#34;D8-11000&#34; or             @code=&#34;D8-70110&#34; or             @code=&#34;D8-12000&#34; or             @code=&#34;CSE-050&#34; or             @code=&#34;F-87000&#34; or             @code=&#34;XX-MCH135&#34;             ]">
            
            [CSEpregnacyHistoryEntries] L'attribut 'code' doit être codé selon les valeurs prévues dans le volet. 
        </assert>

        <assert test="not(cda:code[@code=&#34;XX-MCH027&#34;]) or cda:value[@xsi:type=&#34;CD&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "date de déclaration de grossesse" 
            est une donnée codée (@xsi:type="CD").           
        </assert>
    
        <assert test="not(cda:code[@code=&#34;11977-6&#34;]) or cda:value[@xsi:type=&#34;INT&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Gestité" s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test="not(cda:code[@code=&#34;11640-0&#34;]) or cda:value[@xsi:type=&#34;INT&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Parité" s'exprime en entier (@xsi:type="INT").           
        </assert>
        <assert test="not(cda:code[@code=&#34;11637-6&#34;]) or cda:value[@xsi:type=&#34;INT&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Nb d'enfants nés avant 37 semaines" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test="not(cda:code[@code=&#34;XX-MCH182&#34;]) or cda:value[@xsi:type=&#34;INT&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Nb d'enfants pesant moins de 2500g" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test="not(cda:code[@code=&#34;XX-MCH183&#34;]) or cda:value[@xsi:type=&#34;INT&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Nb d'enfants morts-nés" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test="not(cda:code[@code=&#34;XX-MCH019&#34;]) or cda:value[@xsi:type=&#34;INT&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Nb d'enfants morts avant 28 jours" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test="not(cda:code[@code=&#34;11878-6&#34;]) or cda:value[@xsi:type=&#34;INT&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Nb de Foetus" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test="not(cda:code[@code=&#34;11884-4&#34;]) or cda:value[@unit=&#34;wk&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "L'âge gestationnel" 
            s'exprime en semaines (value[@unit="wk"]).
        </assert>
        <assert test="not(cda:code[@code=&#34;D8-20432&#34;]) or cda:value[@xsi:type=&#34;BL&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Antécédents de Césarienne" 
            est un booléen (@xsi:type="BL").
        </assert>
        <assert test="not(cda:code[@code=&#34;P2-87524&#34;]) or cda:value[@xsi:type=&#34;BL&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Transfert in utero" 
            est un booléen (@xsi:type="BL").
        </assert>
        <assert test="not(cda:code[@code=&#34;XX-MCH029&#34;]) or cda:value[@xsi:type=&#34;INT&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Rang de naissance" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test="not(cda:code[@code=&#34;XX-MCH035&#34;]) or cda:value[@xsi:type=&#34;BL&#34;]">
            [CSEpregnacyHistoryEntries] l'élément "Préparation à la naissance" 
            est un booléen (@xsi:type="BL").
        </assert>
    </rule>
</pattern>
    
    <pattern id="CSEhistoryOfPastIllnessEnt-errors">
    <title>IHE PCC v3.0 History of Past Illness Section</title>
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.3.8&#34;]"> 
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.1']) or             .//cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='DF-00006'"> 
            Erreur de Conformité volet CSE: Le test "Pathologie en cours de grossesse" doit être présent 
        </assert>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.1']) or             .//cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='D8-110F9'"> 
            Erreur de Conformité volet CSE: Le test "Prééclampsie" doit être présent 
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.1']) or             .//cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='D8-11120'"> 
            Erreur de Conformité volet CSE: Le test "HTA traitée" doit être présent 
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.1']) or             .//cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='DB-61400'"> 
            Erreur de Conformité volet CSE: Le test "Diabète gestationnel" doit être présent 
        </assert>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.3'] or /cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.2']) or             (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='XX-MCH129')"> 
            Erreur de Conformité volet CSE: Le test "Prématurité inf. à 33 semaines" doit être présent 
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.3'] or /cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.2']) or             (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='XX-MCH039')"> 
            Erreur de Conformité volet CSE: Le test "Otites à répétition" doit être présent 
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.3'] or /cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.2']) or             (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='XX-MCH040')"> 
            Erreur de Conformité volet CSE: Le test "Affections bronchopulmonaires à répétition" doit être présent 
        </assert>
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.3']) or              (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='XX-MCH141')"> 
            Erreur de Conformité volet CSE: Le test "Affections bronchopulmonaires sifflantes &gt;3" doit être présent 
        </assert>  
        
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.3']) or              (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='XX-MCH142')"> 
            Erreur de Conformité volet CSE: Le test 'Accidents domestiques avant le 9ème mois' doit être présent 
        </assert>       
        <report test="((cda:entry/cda:act/cda:entryRelationship/cda:observation[cda:code/@code=&#34;XX-MCH142&#34;]/cda:value/@value=&#34;true&#34;) and             (cda:entry/cda:act/cda:entryRelationship/cda:observation[cda:code/@code=&#34;XX-MCH142&#34;]//cda:entryRelationship/@typeCode!=&#34;CAUS&#34;))"> 
            Erreur de Conformité volet CSE: La cause de la pathologie s'exprime à partir d'un élément entryRelationship d'attribut typeCode='CAUS'
        </report> 
     
        

        

        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.3']) or             (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:entryRelationship/cda:observation/cda:code/@code='XX-MCH143')"> 
            Erreur de Conformité volet CSE: Le test "Nombre d'hospitalisations depuis le 9ème mois" doit être présent 
        </assert>
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.3']) or             (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation/cda:value/@code='XX-MCH144')"> 
            Erreur de Conformité volet CSE: Le test "causes d'hospitalisations depuis le 9ème mois" doit être présent  
        </assert>
        
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.2']) or              (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:value/@code='XX-MCH036')"> 
            Erreur de Conformité volet CSE: Le test 'Accidents domestiques avant le 9ème mois' doit être présent 
        </assert>       
        <report test="((cda:entry/cda:act/cda:entryRelationship/cda:observation[cda:code/@code=&#34;XX-MCH036&#34;]/cda:value/@value=&#34;true&#34;) and             (cda:entry/cda:act/cda:entryRelationship/cda:observation[cda:code/@code=&#34;XX-MCH036&#34;]//cda:entryRelationship/@typeCode!=&#34;CAUS&#34;))"> 
            Erreur de Conformité volet CSE: La cause de la pathologie s'exprime à partir d'un élément entryRelationship d'attribut typeCode='CAUS'
        </report> 
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.2']) or             (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:entryRelationship/cda:observation/cda:code/@code='XX-MCH137')"> 
            Erreur de Conformité volet CSE: Le test "Nombre d'hospitalisations en période néonatale" doit être présent 
        </assert>
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.2']) or             (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation/cda:value/@code='XX-MCH139')"> 
            Erreur de Conformité volet CSE: Le test "causes d'hospitalisations en période néonatale" doit être présent  
        </assert>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.2']) or             (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5']/cda:entryRelationship/cda:observation/cda:code/@code='XX-MCH138')"> 
            Erreur de Conformité volet CSE: Le test "Nombre d'hospitalisations après la période néonatale" doit être présent 
        </assert>

        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root='1.2.250.1.213.1.1.1.5.2']) or             (cda:entry/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship/cda:observation/cda:value/@code='XX-MCH140')"> 
            Erreur de Conformité volet CSE: Le test "causes d'hospitalisations après la période néonatale" doit être présent  
        </assert>
    </rule> 
</pattern>
    <pattern id="CSEmusculoEnt-errors">
    <title>IHE PCC v3.0 Musculoskeletal System</title>
    
    
    <rule context="*[cda:templateId/@root=&#34;1.3.6.1.4.1.19376.1.5.3.1.1.9.34&#34;]">
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.1&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-12102&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence de Réduction d'un membre ou absence d'éléments osseux doit obligatoirement 
            être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D4-14700&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence de luxation de la hanche doit obligatoirement 
            être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        
        <assert test="not(/cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.3&#34;] or /cda:ClinicalDocument/cda:templateId[@root=&#34;1.2.250.1.213.1.1.1.5.2&#34;]) or             .//cda:entry/cda:observation[@negationInd]/cda:value/@code=&#34;D1-00000&#34;">
            Erreur de Conformité volet CSE: L'absence ou la présence d'une autre affection du système
            doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        <report test="(.//cda:entry/cda:observation[@negationInd=&#34;false&#34;]/cda:value/@code=&#34;D1-00000&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;D1-00000&#34;]/cda:entryRelationship/cda:act/cda:text=&#34;&#34;)">
            Erreur de Conformité volet CSE: Dans le cas d'une autre pathologie, préciser laquelle 
        </report>
        <report test="(.//cda:entry/cda:observation[@negationInd=&#34;true&#34;]/cda:value/@code=&#34;D1-00000&#34;) and              (.//cda:entry/cda:observation[cda:value/@code=&#34;D1-00000&#34;]/cda:entryRelationship/cda:act/cda:text!=&#34;&#34;)"> 
            Erreur de Conformité volet CSE: Dans le cas d'une autre affection du système signalée, 
            le booléen cda:observation/@negationInd doit être fixé à la valeur "false" 
            sinon l'élément cda:entry/cda:observation/cda:entryRelationship/cda:act/cda:text doit être vide
        </report>

    </rule>
</pattern>
    

    
                                        
  
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="activProf">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="/cda:ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section/cda:entry/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4' and cda:code/@code='G-D786']/cda:value">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/component/structuredBody/component/section/entry/observation/value'"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/component/structuredBody/component/section/entry/observation/value'"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_activProf)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="'ClinicalDocument/component/structuredBody/component/section/entry/observation/value'"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_activProf"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="catProf">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="/cda:ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section/cda:entry/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4' and cda:code/@code='J-00000']/cda:value">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/component/structuredBody/component/section/entry/observation/value'"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/component/structuredBody/component/section/entry/observation/value'"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_catProf)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="'ClinicalDocument/component/structuredBody/component/section/entry/observation/value'"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_catProf"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="dateCons">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="/cda:ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section/cda:entry/cda:observation[cda:code/@code='xx-MCH027']/cda:value">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section/cda:entry/cda:observation/cda:value"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section/cda:entry/cda:observation/cda:value"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_dateCons)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section/cda:entry/cda:observation/cda:value"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_dateCons"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="demConsult">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="/cda:ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.31']/cda:entry/cda:encounter/cda:code">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/component/structuredBody/component/section/entry/encounter/code'"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/component/structuredBody/component/section/entry/encounter/code'"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_demConsult)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="'ClinicalDocument/component/structuredBody/component/section/entry/encounter/code'"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_demConsult"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="nivEtude">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="/cda:ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section/cda:entry/cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4' and cda:code/@code='S-00610']/cda:value">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 1)">
           [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/author/assignedAuthor/code'"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (1 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="'ClinicalDocument/author/assignedAuthor/code'"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_nivEtude)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="'ClinicalDocument/author/assignedAuthor/code'"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_nivEtude"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="rechAgHbS">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="/cda:ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section/cda:entry/cda:organizer/cda:component/cda:observation[cda:code/@code='5196-1']/cda:value">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/cda:value"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/cda:value"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_rechAgHbS)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/cda:value"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_rechAgHbS"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="typPres">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="/cda:ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section/cda:entry/cda:observation[cda:code/@code='F-87000']/cda:value">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 1)">
           [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/entry/observation/value"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (1 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/entry/observation/value"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_typPres)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="ClinicalDocument/component/structuredBody/component/section/entry/observation/value"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_typPres"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="HealthStatusCodes">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.1.2']/cda:value">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="entryRelationship/observation/value"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="entryRelationship/observation/value"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_HealthStatusCodes)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="entryRelationship/observation/value"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_HealthStatusCodes"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="ClinicalStatusCodes">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.1.1']/cda:value">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation/entryRelationship/observation/entryRelationship/observation/value"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation/entryRelationship/observation/entryRelationship/observation/value"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_ClinicalStatusCodes)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation/entryRelationship/observation/entryRelationship/observation/value"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_ClinicalStatusCodes"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="ProblemCodes">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5' and not (cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.6')]/cda:code">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation/code"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation/code"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_ProblemCodes)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation/code"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_ProblemCodes"/>.
        </assert>
    </rule>
</pattern>
    <!--Start pattern based on abstract dansJeuDeValeurs--><pattern xmlns:svs="urn:ihe:iti:svs:2008" id="AllergyAndIntoleranceCodes">
    <p>Conformité d'un élément codé obligatoire par rapport à un jeu de valeurs du CI-SIS</p>
    <rule context="cda:observation[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.6']/cda:code">
        <let name="att_code" value="@code"/>
        <let name="att_codeSystem" value="@codeSystem"/>
        <let name="att_displayName" value="@displayName"/>
        
        <assert test="(not(@nullFlavor) or 0)">
           [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation/code"/>" ne doit pas comporter d'attribut nullFlavor.
        </assert> 
        
        <assert test="(             (@code and @codeSystem and @displayName) or             (0 and              (@nullFlavor='UNK' or              @nullFlavor='NASK' or              @nullFlavor='ASKU' or              @nullFlavor='NI' or              @nullFlavor='NAV' or              @nullFlavor='MSK' or              @nullFlavor='OTH')) or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE'))             )">
            [dansJeuDeValeurs] L'élément "<value-of select="ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation/code"/>" doit avoir ses attributs 
            @code, @codeSystem et @displayName renseignés, ou si le nullFlavor est autorisé, une valeur admise pour cet attribut, ou un xsi:type différent de CD ou CE.
        </assert>
        
        <assert test="(             @nullFlavor or             (@xsi:type and not(@xsi:type = 'CD') and not(@xsi:type = 'CE')) or              (document($jdv_AllergyAndIntoleranceCodes)//svs:Concept[@code=$att_code and @codeSystem=$att_codeSystem])             )">
        
            [dansJeuDeValeurs] L'élément <value-of select="ClinicalDocument/component/structuredBody/component/section/component/section/entry/observation/code"/>
            [<value-of select="$att_code"/>:<value-of select="$att_displayName"/>:<value-of select="$att_codeSystem"/>] 
            doit faire partie du jeu de valeurs <value-of select="$jdv_AllergyAndIntoleranceCodes"/>.
        </assert>
    </rule>
</pattern>
    
    
        
    
    
    <phase id="CS8-20130326">
        <active pattern="variables"/>
        
        

        <active pattern="CS8ModeleEnTete"/>
        <active pattern="addr"/>
        <active pattern="administrativeGenderCode"/>
        <active pattern="assignedAuthor"/>       
        <active pattern="assignedEntity"/>
        <active pattern="authenticatorName"/>
        <active pattern="authorPersonName"/>
        <active pattern="authorSpecialty"/>
        <active pattern="authorTime"/>
        <active pattern="documentCode"/>
        <active pattern="documentEffectiveTime"/>
        <active pattern="healthcareFacilityTypeCode"/>
        <active pattern="informantAssignedPersonName"/>
        <active pattern="informantRelatedEntity"/>
        <active pattern="legalAuthenticatorName"/>
        <active pattern="legalAuthenticatorTime"/>
        <active pattern="modeleCommunEnTete"/>
        <active pattern="patient"/>
        <active pattern="patientBirthTime"/>
        <active pattern="patientId"/>
        <active pattern="patientName"/>
        <active pattern="practiceSettingCode"/>
        <active pattern="relatedDocument"/>
        <active pattern="relatedPersonName"/>
        <active pattern="representedCustodianOrganization"/>
        <active pattern="serviceEventEffectiveTime"/>
        <active pattern="serviceEventPerformer"/>
        <active pattern="telecom"/>   
        
        
        
                                     
        
        <active pattern="codedVitalSigns-errors"/>
        
        <active pattern="activeProblemSection-errors"/>
        <active pattern="assessmentAndPlan-errors"/>
        <active pattern="carePlan-errors"/>
        <active pattern="CodedAntenatalTestingAndSurveillance-errors"/>
        <active pattern="codedAntenatalTestingAndSurveillanceOrg-errors"/>
        <active pattern="codedPhysicalExam-errors"/>
        <active pattern="codedResults-errors"/>
        <active pattern="codedSocialHistory-errors"/>
        
        <active pattern="EarsPhysicalExam-errors"/>
        <active pattern="encounterHistoriesSection-errors"/>
        <active pattern="generalAppearancePhysicalExam-errors"/>
        <active pattern="genitaliaPhysicalExam-errors"/>
        <active pattern="heartPhysicalExam-errors"/>
        <active pattern="historyOfPastIllness-errors"/>
        <active pattern="immunizations-errors"/>
        <active pattern="laborAndDeliverySection-errors"/>
        <active pattern="musculoPhysicalExam-errors"/>
        <active pattern="neurologicPhysicalExam-errors"/>
        <active pattern="pregnancyHistorySection-errors"/>
        <active pattern="prenatalEvents-errors"/>
        <active pattern="problemConcernEntry-errors"/>
        <active pattern="problemEntry-errors"/>
        <active pattern="proceduresSection-errors"/>      
            
                                 

        
        <active pattern="CS8codedPhysicalExam-errors"/>
        <active pattern="CSEcodedResultsSubsections-errors"/>               
        
                                 
        
        
        <active pattern="ClinicalStatusCodes"/>
        <active pattern="codedAntenatalTestingAndSurveillanceOrg-errors"/> 
        <active pattern="codedVitalSignsOrg-errors"/>
        <active pattern="comments-errors"/>
        <active pattern="concernEntry-errors"/>
        <active pattern="encountersEntry-errors"/>
        <active pattern="HealthStatusCodes"/>
        <active pattern="immunizationsEnt-errors"/>
        <active pattern="observationInterpretation"/>
        <active pattern="ProblemCodes"/>
        <active pattern="AllergyAndIntoleranceCodes"/>
        <active pattern="problemConcernEntry-errors"/>
        <active pattern="problemEntry-errors"/>
        <active pattern="procedureEntry-errors"/>
        <active pattern="simpleObservation-errors"/>       
        
                                 
        
        <active pattern="CSECodedVitalSignsEnt-errors"/>
        <active pattern="CSEabdomenEnt-errors"/>
        <active pattern="CSECodedAntenatalTestingAndSurveillanceEnt-errors"/>
        <active pattern="CSEcodedResultsEntries-errors"/>
        <active pattern="CSEcodedSocialHistoryEnt-errors"/>
        <active pattern="CSERechHbs-errors"/>
        <active pattern="CSECatProf-errors"/>
        <active pattern="CSEearsEnt-errors"/>
        <active pattern="CSEgeneralAppEnt-errors"/>
        <active pattern="CSEGenitaliaEnt-errors"/>
        <active pattern="CSEHeartEnt-errors"/>
        <active pattern="CSEhistoryOfPastIllnessEnt-errors"/>
        <active pattern="CSEimmunizationsEnt-errors"/>
        <active pattern="CSElaborAndDeliveryEnt-errors"/>
        <active pattern="CSEmusculoEnt-errors"/>
        <active pattern="CSENeurogicEnt-errors"/>
        <active pattern="CSEpregnancyHistoryEntries-errors"/>

                                        
        <active pattern="nivEtude"/>
        
        <active pattern="activProf"/>
        
        
        <active pattern="demConsult"/>
        <active pattern="dateCons"/>
        <active pattern="typPres"/>
        <active pattern="typPres"/>
        <active pattern="HealthStatusCodes"/>
        <active pattern="ClinicalStatusCodes"/>
        <active pattern="ProblemCodes"/>
        <active pattern="AllergyAndIntoleranceCodes"/>
    </phase>
                                        
    
    <pattern id="variables">
        <let name="enteteHL7France" value="'2.16.840.1.113883.2.8.2.1'"/>
        
        <let name="commonTemplate" value="'1.2.250.1.213.1.1.1.1'"/>
        
        <let name="XDS-SD" value="'1.3.6.1.4.1.19376.1.2.20'"/>
        
        <let name="OIDphysique" value="'1.2.250.1.71.4.2.1'"/>
        
        <let name="OIDmorale" value="'1.2.250.1.71.4.2.2'"/>
        
        <let name="OIDINS-c" value="'1.2.250.1.213.1.4.2'"/>
        
        
        
        <let name="jdv_typeCode" value="'../jeuxDeValeurs/CI-SIS_jdv_typeCode.xml'"/>
        <let name="jdv_practiceSettingCode" value="'../jeuxDeValeurs/CI-SIS_jdv_practiceSettingCode.xml'"/>
        <let name="jdv_healthcareFacilityTypeCode" value="'../jeuxDeValeurs/CI-SIS_jdv_healthcareFacilityTypeCode.xml'"/>
        <let name="jdv_authorSpecialty" value="'../jeuxDeValeurs/CI-SIS_jdv_authorSpecialty.xml'"/>
        <let name="jdv_activProf" value="'../jeuxDeValeurs/CI-SIS_jdv_activProf.xml'"/>
        <let name="jdv_nivEtude" value="'../jeuxDeValeurs/CI-SIS_jdv_nivEtude.xml'"/>
        
        
        
        <let name="jdv_demConsult" value="'../jeuxDeValeurs/CI-SIS_jdv_demConsult.xml'"/>
        <let name="jdv_dateCons" value="'../jeuxDeValeurs/CI-SIS_jdv_dateCons.xml'"/>
        <let name="jdv_typPres" value="'../jeuxDeValeurs/CI-SIS_jdv_typPres.xml'"/>
        <let name="jdv_observationInterpretation" value="'../jeuxDeValeurs/CI-SIS_jdv_observationInterpretation.xml'"/>
        <let name="jdv_HealthStatusCodes" value="'../jeuxDeValeurs/CI-SIS_jdv_HealthStatusCodes.xml'"/>
        <let name="jdv_ClinicalStatusCodes" value="'../jeuxDeValeurs/CI-SIS_jdv_ClinicalStatusCodes.xml'"/>
        <let name="jdv_ProblemCodes" value="'../jeuxDeValeurs/CI-SIS_jdv_ProblemCodes.xml'"/>
        <let name="jdv_AllergyAndIntoleranceCodes" value="'../jeuxDeValeurs/CI-SIS_jdv_AllergyAndIntoleranceCodes.xml'"/>
    </pattern>
</schema>