<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    FRCP_Tumeur20140109.sch :
    Contenu :

    Paramètres d'appel :
        Néant 
    Historique :
        27/06/11 : CRI ASIP/PRAS : Création
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="FRCP_Tumeur" is-a="dansJeuDeValeurs">
    <p>Conformité des demandes d'examen complémentaires</p>
    <param name="path_jdv" value="$jdv_FRCP_Tumeur"/>
    <param name="vue_elt" value="ClinicalDocument/component/structuredBody/component/section/entry/observation[value/@code='RCP_026']/entryRelationship/observation/entryRelationship/observation/targetSiteCode"/>
    <param name="xpath_elt" value="/cda:ClinicalDocument/cda:component/cda:structuredBody/cda:component/cda:section[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.2']/cda:entry/cda:observation[cda:value/@code='RCP_026']/cda:entryRelationship/cda:observation/cda:entryRelationship/cda:observation/cda:targetSiteCode"/>
    <param name="nullFlavor" value="0"/>
</pattern>   

