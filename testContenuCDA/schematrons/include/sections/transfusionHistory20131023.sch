<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ transfusionHistory20131023.sch ]O@%#o>>=-
   
   Teste la conformité de la section " IHE PCC Transfusion History Section : 1.3.6.1.4.1.19376.1.5.3.1.1.9.12
   aux spécifications d'IHE PCC v3.0
   
   Historique :
   23/10/13 : CRI : Création
   
-->

<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="transfusionHistory-errors"> 
   <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.1.9.12"]'> 
      <!-- Verify that the template id is used on the appropriate type of object --> 
      <assert test='../cda:section'> 
         Error: The Transfusion History can only be used on sections. 
      </assert> 
      <!-- Verify the section type code --> 
      <assert test='cda:code[@code = "56836-0"]'> 
         Error: The section type code of a Transfusion History must be TBD 
      </assert> 
      <assert test='cda:code[@codeSystem = "2.16.840.1.113883.6.1"]'> 
         Error: The section type code must come from the LOINC code  
         system (2.16.840.1.113883.6.1). 
      </assert> 
   </rule> 
</pattern>

