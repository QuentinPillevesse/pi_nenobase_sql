<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ FRCP_codedReasonForReferral.sch ]O@%#o>>=-
    
    Teste la conformité du contenu de la section "Raison de la recommandation" (1.3.6.1.4.1.19376.1.5.3.1.3.2)
    aux spécifications d'IHE PCC v3.0
    
    Historique :
    25/07/11 : CRI : Création
    14/01/13 : CRI : Modification pour intégrer les catégories et classes de la raison
    
-->


<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="FRCP_codedReasonForReferral-errors">
    <title>IHE PCC v3.0 Procedures Section</title>

    <title>IHE PCC v3.0 FRCP_codedReasonForReferral Section</title>
        
        
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.3.2"]'> 
            
                            <!-- -=<<o#%@O[ But de la présentation à la RCP ]O@%#o>>=-  -->
        
        <!-- Teste la présence -->
        <assert test="./cda:entry/cda:observation/cda:code[@code='RCP_105']">
            Erreur [FRCP_codedReasonfForReferral]:  Le but de la présentation à la RCP (RCP_105) doit être figuré.
        </assert>
        <!-- Teste les valeurs -->
        <assert test="not(./cda:entry/cda:observation/cda:code[@code='RCP_105']) or ./cda:entry/cda:observation/cda:value[@code='RCP_103' or @code='RCP_104'] ">
            Erreur [FRCP_codedReasonfForReferral]:  La valeur donnée pour coder le but de la présentation à la RCP ne fait pas partie des valeurs permises.
        </assert>
        
                            <!-- -=<<o#%@O[ Nature de la discussion à la RCP ]O@%#o>>=-  -->
        
        <!-- Teste la présence -->
        <assert test="./cda:entry/cda:observation/cda:entryRelationship/cda:observation/cda:code[@code='RCP_100']">
            Erreur [FRCP_codedReasonfForReferral]:  La nature de la discussion de la RCP (RCP_100)doit être présente. 
        </assert>
        <!-- Teste le type d'entryRelationship de type 'SUBJ'-->       
        <assert test="not(./cda:entry/cda:observation/cda:entryRelationship/cda:observation/cda:code[@code='RCP_100']) or 
            ./cda:entry/cda:observation/cda:entryRelationship[@typeCode='SUBJ']/cda:observation/cda:code[@code='RCP_100']">
            Erreur [FRCP_codedReasonfForReferral]:  La valeur de @typeCode est @typeCode='SUBJ'. 
        </assert>
        <!-- Teste les valeurs -->
        <assert test="not(./cda:entry/cda:observation/cda:entryRelationship[@typeCode='SUBJ']/cda:observation/cda:code[@code='RCP_100']) or
            ./cda:entry/cda:observation/cda:entryRelationship[@typeCode='SUBJ']/cda:observation/cda:value[@code='RCP_026' or @code='RCP_027']">
            Erreur [FRCP_codedReasonfForReferral]: La valeur donnée pour coder la nature de la discussion de la RCP ne fait pas partie des valeurs permises.
        </assert>
        
                             <!-- -=<<o#%@O[ Catégorie de la RCP ]O@%#o>>=-  -->        
        
        <!-- Teste la présence -->
        <assert test="./cda:entry/cda:observation/cda:entryRelationship/cda:observation/cda:code[@code='RCP_085']">
            Erreur [FRCP_codedReasonfForReferral]: La catégorie de la RCP (RCP_027) doit être figurée. 
        </assert>
        <!-- Teste le type d'entryRelationship de type 'REFR'-->    
        <assert test="not(./cda:entry/cda:observation/cda:entryRelationship/cda:observation/cda:code[@code='RCP_085']) or 
            ./cda:entry/cda:observation/cda:entryRelationship[@typeCode='REFR']/cda:observation/cda:code[@code='RCP_085']">
            Erreur [FRCP_codedReasonfForReferral]: La valeur de @typeCode est @typeCode='REFR'. 
        </assert>
        <!-- Teste les valeurs -->
        <assert test="not(./cda:entry/cda:observation/cda:entryRelationship/cda:observation/cda:code[@code='RCP_085']) or( 
            ./cda:entry/cda:observation/cda:entryRelationship/cda:observation/cda:value[@code='RCP_093' or
            @code='RCP_094' or
            @code='RCP_095' or
            @code='RCP_096' or
            @code='M-88003' or
            @code='RCP_097' or
            @code='RCP_078' or
            @code='RCP_098' or
            @code='D1-F0106' or
            @code='F-A2600' or
            @code='P2-00200' or
            @code='P0-0044D'])">
            Erreur [FRCP_codedReasonfForReferral]: La valeur donnée pour coder la catégorie de la RCP ne fait pas partie des valeurs permises.
        </assert>

                            <!-- -=<<o#%@O[ Classe de la RCP ]O@%#o>>=-  -->
        
        <!-- Teste le type d'entryRelationship de type 'REFR'-->    
        <assert test="not(./cda:entry/cda:observation/cda:entryRelationship/cda:observation/cda:code[@code='RCP_096']) or 
            ./cda:entry/cda:observation/cda:entryRelationship[@typeCode='REFR']/cda:observation/cda:code[@code='RCP_096']">
            Erreur [FRCP_codedReasonfForReferral]: La valeur de @typeCode est @typeCode='REFR'. 
        </assert>
        
    </rule>
</pattern>