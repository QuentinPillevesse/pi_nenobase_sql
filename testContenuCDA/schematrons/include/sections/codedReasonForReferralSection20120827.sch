<!--  IHE PCC Coded Reason for Referral Section: 1.3.6.1.4.1.19376.1.5.3.1.3.2 -->


<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="codedReasonForReferral-errors">
    <title>IHE PCC Minimum Data Set Section</title>
    
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.3.2"]'> 
        <!-- Verifier que le templateId est utilisé pour une section -->
        <assert test='../cda:section'> 
            Erreur [codedReasonForReferralSection]: Ce template ne peut être utilisé que pour une section. 
        </assert>

        <!-- Verifier la présence du templateId parent. --> 
        <assert test='cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.3.1"]'> 
            Erreur [codedReasonForReferralSection]: Le templateiD parent est absent. 
        </assert> 
        
        <!-- Vérifier le code de la section --> 
        <assert test='cda:code[@code = "42349-1"]'> 
            Erreur [codedReasonForReferralSection]: Le code de la section 'Coded Reason for Referral' doit être 42349-1
        </assert> 
        
        <assert test='cda:code[@codeSystem = "2.16.840.1.113883.6.1"]'> 
            Erreur [codedReasonForReferralSection]: L'élément 'codeSystem' de la section 'Coded Reason for Referral' 
            doit être codé à partir de la nomenclature LOINC 
            (2.16.840.1.113883.6.1). 
        </assert> 
        
        <assert test='cda:title'>
            Erreur [codedReasonForReferralSection]: Cette section doit avoir un titre significatif de son contenu.
        </assert>
        
        <assert test='cda:id'>
            Erreur [codedReasonForReferralSection]: Les sections doivent avoir un identifiant unique permettant de les identifier.
        </assert>
        


        
    </rule>
    
</pattern>

