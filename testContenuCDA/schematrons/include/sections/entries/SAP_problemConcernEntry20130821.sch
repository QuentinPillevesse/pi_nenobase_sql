<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ SAP_problemConcernEntry.sch ]O@%#o>>=-
    
    Teste la conformité de l'élément "Problem Concern Entry" (1.3.6.1.4.1.19376.1.5.3.1.4.5.2)
    aux spécifications du volet SAP
    
    Historique :
    21/008/13 : CRI : Création
    
-->



<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="SAP_problemConcernEntry-errors">
    <title>IHE PCC v3.0 Problem Concern Entry - errors validation phase</title>
    
    <rule context="*[(cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.6')]">
    
        <assert test="(cda:code/@code='11450-4') and (cda:entry/cda:act/cda:entryRelationship/cda:observation/cda:value/@code='OBS_016')"> 
            [SAP_problemConcernEntry] L'élément "Métrorragies 2-3e trimestre" (code OBS_016) doit faire partie des éléments à vérifier
        </assert>
        <assert test="(cda:code/@code='11450-4') and (cda:entry/cda:act/cda:entryRelationship/cda:observation/cda:value/@code='D8-12000')"> 
            [SAP_problemConcernEntry] L'élément "MAP" (code D8-12000) doit faire partie des éléments à vérifier
        </assert>    
        <assert test="(cda:code/@code='11450-4') and (cda:entry/cda:act/cda:entryRelationship/cda:observation/cda:value/@code='OBS_017')"> 
            [SAP_problemConcernEntry] L'élément "Rupture prématurée des membranes" (code OBS_017) doit faire partie des éléments à vérifier
        </assert> 
        <assert test="(cda:code/@code='11450-4') and (cda:entry/cda:act/cda:entryRelationship/cda:observation/cda:value/@code='D8-11000')"> 
            [SAP_problemConcernEntry] L'élément "Hypertension artérielle" (code D8-11000) doit faire partie des éléments à vérifier 
        </assert> 
        <assert test="(cda:code/@code='11450-4') and (cda:entry/cda:act/cda:entryRelationship/cda:observation/cda:value/@code='E14')"> 
            [SAP_problemConcernEntry] L'élément "Diabète" (code E14) doit faire partie des éléments à vérifier 
        </assert> 
        <assert test="(cda:code/@code='11450-4') and (cda:entry/cda:act/cda:entryRelationship/cda:observation/cda:value/@code='D7-10030')"> 
            [SAP_problemConcernEntry] L'élément "Infection urinaire" (code D8-12000)doit faire partie des éléments à vérifier 
        </assert> 
        <assert test="(cda:code/@code='11450-4') and (cda:entry/cda:act/cda:entryRelationship/cda:observation/cda:value/@code='OBS_132')"> 
            [SAP_problemConcernEntry] L'élément "Infection Cervico-Vaginale" (code OBS_132)doit faire partie des éléments à vérifier 
        </assert> 
        <assert test="(cda:code/@code='11450-4') and (cda:entry/cda:act/cda:entryRelationship/cda:observation/cda:value/@code='OBS_016') and (cda:entry/cda:act/cda:entryRelationship/cda:observation/@negationInd)"> 
            [SAP_problemConcernEntry] L'élément "Métrorragies 2-3e trimestre" (code OBS_016) doit faire partie des éléments à vérifier
        </assert>
    </rule>
</pattern>
