<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ problemConcernEntry.sch ]O@%#o>>=-
    
    Teste la conformité de l'élément "Problem Concern Entry" (1.3.6.1.4.1.19376.1.5.3.1.4.5.2)
    aux spécifications d'IHE PCC v3.0
    
    Historique :
    24/06/11 : CRI : Création
    
-->



<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="problemConcernEntry-errors">
    <title>IHE PCC v3.0 Problem Concern Entry - errors validation phase</title>
    <rule context="*[cda:templateId/@root=&quot;1.3.6.1.4.1.19376.1.5.3.1.4.5.2&quot;]">

        <assert test="cda:templateId[@root = &quot;1.3.6.1.4.1.19376.1.5.3.1.4.5.1&quot;]"> 
            [problemConcernEntry] Problem Concern Entry a un template OID 1.3.6.1.4.1.19376.1.5.3.1.4.5.2. 
            Elle spécialise Concern Entry et doit donc se conformer à ses spécifications 
            en déclarant son template OID qui est 1.3.6.1.4.1.19376.1.5.3.1.4.5.1. Ces éléments 
            sont requis.
        </assert>
        
        <assert test="cda:templateId[@root = &quot;2.16.840.1.113883.10.20.1.27&quot;]"> 
            [problemConcernEntry] Le template parent de Problem Concern est absent.
        </assert>
<!-- 
        <assert
            test="(cda:entryRelationship[@typeCode = &quot;SUBJ&quot;] and
                cda:entryRelationship//cda:templateId[@root=&quot;1.3.6.1.4.1.19376.1.5.3.1.4.5&quot;] and
                cda:entryRelationship[@inversionInd=&quot;false&quot;])"> 
            [problemConcernEntry] Problem Concern Entry contiendra une ou plusieurs entrées qui se conformeront 
            au template Problem Entry (1.3.6.1.4.1.19376.1.5.3.1.4.5) et qui se matérialiseront sous 
            la forme d'éléments 'entryRelationship'. L'attribut 'typeCode' prend la valeur 'SUBJ'
            et l'attribut 'inversionInd' prend la valeur 'false'. 
        </assert> -->
    </rule>
</pattern>
