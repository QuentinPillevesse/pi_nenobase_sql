<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ AllergiesAndIntolerances.sch ]O@%#o>>=-
    
    Teste la conformité d'une entrée utilisée dans le volet du CI-SIS aux spécifications de l'entrée 
    PCC allergies and Intolerances (1.3.6.1.4.1.19376.1.5.3.1.4.6)  
    
    
    Historique :
    02/08/11 : CRI : Création
    
-->

<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="AllergiesAndIntolerances-errors">

    <title>IHE PCC v3.0 Allergies and Intolerances</title>
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.4.6"]'>
        
        <assert test='../cda:observation[@classCode="OBS"]'>
            Erreur de Conformité PCC: Une entrée 'Allergies and intolerances' est un type particulier 
            de problème et sera de la même manière représentée comme une élément de type 'observation' 
            avec un attribut classCode='OBS'.</assert>
        
        <assert test='cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.5"]'>
            Erreur de Conformité PCC: Le template de l'entrée 'Allergies and intolerances' spécialise le template 
            de l'entrée 'Problem Entry'. A ce titre, le templateId de ce dernier (1.3.6.1.4.1.19376.1.5.3.1.4.5) sera déclaré.</assert>
        
        <assert test='cda:code[@code and @codeSystem]'>
            Erreur de Conformité PCC: L'élément 'code' de l'entrée 'Allergies and Intolerances' indique le 
            type d'allergie provoqué (par un médicament, un facteur environnemental ou un aliment), 
            s'il s'agit d'une allergie, d'une intolérance sans manifestation allergique, ou encore un
            type inconnu de réaction (ni allergique, ni intolérance).
            L'élément 'code' doit obligatoirement comporter les attributs 'code' et 'codeSystem'.</assert>
        
        <assert test='(cda:value[@xsi:type="CD"]) and 
            (cda:value[@code and @codeSystem] or cda:value[not(@code) and not(@codeSystem)])'>
            Erreur de Conformité PCC: L'élément 'value' décrit l'allergie ou la réaction adverse observée. 
            Alors que l'élément 'value' peut être un caractère codé ou non, son type sera 
            toujours 'coded value' (xsi:type='CD'). 
            Dans le cas de l'utilisation d'un code, les attributs les attributs 'code' et 'codeSystem'seront présents, 
            et dans le cas contraire, tout autre attribut que xsi:type='CD' seront absents.</assert>
        
        <assert test='//cda:code/cda:originalText/cda:reference'>
            Erreur de Conformité PCC: L'élément 'code' dans une entrée 'Allergies and Intolerances' sera présent. Il pourra contenir 
            des atributs 'code' et 'codeSystem' pour identifier la substance causant l'allergie ou l'intolérance. 
            Il contiendra un élément 'reference' pointant vers l'élément 'originalText' de la partie narrative correspondant à
            l'identification du médicament.</assert>
        
        <assert
            test='not(cda:entryRelationship/*/cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.5"]) or
            (cda:entryRelationship/*/cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.5"] and
            cda:entryRelationship/*/cda:templateId[@root="2.16.840.1.113883.10.20.1.54"])'>
            Erreur de Conformité PCC: L'entrée 'Allergies and Intolerance' spécialise l'entrée 
            'Problem Entry' (templateId = 1.3.6.1.4.1.19376.1.5.3.1.4.5) mais doit également être identifiée comme une réaction observée.
            Le template additionnel 'reaction observation' (2.16.840.1.113883.10.20.1.54) doit être déclaré pour cette raison.
            La déclaration de conformité aux templates 'Problem Entry' et 'Reaction Observation' sont obligatoires.</assert>      
        
        <assert
            test='(not(cda:entryRelationship//cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.1"]) and
            not(cda:sourceOf//cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.1"])) or 
            (cda:entryRelationship//cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.1"] and 
            cda:entryRelationship[@typeCode="SUBJ" and @inversionInd="true"]) or
            (cda:sourceOf/*/cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.1"] and 
            cda:sourceOf[@typeCode="SUBJ" and @inversionInd="true"])'>
            Erreur de Conformité PCC: Un élément 'entryRelationship' optionnel peut être présent, indiquant la sévérité du problème observé.
            Cet élément se conformera au template de l'entrée 'Severity' (1.3.6.1.4.1.19376.1.5.3.1.4.1), qui sera déclaré.
            L'attribut 'typeCode' prendra la valeur 'SUBJ' tandis que l'attribut 'inversionInd' prendra la valeur 'true'.</assert>
        
        <assert
            test='(not(cda:entryRelationship//cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.1.1"]) and
            not(cda:sourceOf//cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.1.1"])) or
            (cda:entryRelationship//cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.1.1"] and
            cda:entryRelationship[@typeCode="REFR" and @inversionInd="false"]) or
            (cda:sourceOf/*/cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.1.1"] and
            cda:sourceOf[@typeCode="REFR" and @inversionInd="false"])'>
            Erreur de Conformité PCC: Un élément 'entryRelationship' optionnel peut être présent, indiquant le statut clinique
            de l'allergie observée (résolue, en rémission, etc.).
            Cet élément se conformera au template de l'entrée 'Problem Status Observation' (1.3.6.1.4.1.19376.1.5.3.1.4.1.1), 
            qui sera déclaré.
            L'attribut 'typeCode' prendra la valeur 'REFR' tandis que l'attribut 'inversionInd' prendra la valeur 'false'.</assert>
        
        <assert
            test='(not(cda:entryRelationship//cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.2"]) and
            not(cda:sourceOf//cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.2"])) or
            (cda:entryRelationship//cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.2"] and
            cda:entryRelationship[@typeCode="SUBJ" and @inversionInd="true"]) or
            (cda:sourceOf/*/cda:templateId[@root="1.3.6.1.4.1.19376.1.5.3.1.4.2"] and
            cda:sourceOf[@typeCode="SUBJ" and @inversionInd="true"])'>
            Erreur de Conformité PCC: Un ou plusieurs éléments 'entryRelationship' optionels peuvent être présent, 
            permettant d'apporter des commentaires additionnels concernant la réaction observée.
            Cet élément se conformera au template de l'entrée 'Comment' ((1.3.6.1.4.1.19376.1.5.3.1.4.2), 
            qui sera déclaré.
            L'attribut 'typeCode' prendra la valeur 'SUBJ' tandis que l'attribut 'inversionInd' prendra la valeur 'true'.</assert>

        <!-- warning -->

        <assert test='cda:code[@displayName and @codeSystemName]'>
            Erreur de Conformité PCC (Alerte): L'élément 'code' de l'entrée 'Allergies and Intolerances' indique le 
            type d'allergie provoqué (par un médicament, un facteur environnemental ou un aliment), 
            s'il s'agit d'une allergie, d'une intolérance sans manifestation allergique, ou encore un
            type inconnu de réaction (ni allergique, ni intolérance).
            Les attributs 'displayName' et 'codeSystemName' devraient être présents.</assert>
    </rule>
</pattern>
