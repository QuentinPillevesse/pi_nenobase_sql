<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ pregnancyHistoryOrganizer.sch ]O@%#o>>=-
    
    Teste la conformité de l'élément "PCC Pregnancy History Organizer" (1.3.6.1.4.1.19376.1.5.3.1.4.13.5.1)
    aux spécifications d'IHE PCC v5.0
    
    Historique :
    23/07/12 : CRI : Création
    
-->

<!--  IHE PCC Pregnancy History Organizer: 1.3.6.1.4.1.19376.1.5.3.1.4.13.5.1 -->

<!-- errors -->

<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="pregnancyHistoryOrganizer-errors">
    
    <title>IHE PCC Pregnancy History Organizer - errors validation phase</title>
    
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.4.13.5.1"]'>            
    
        
    </rule>
</pattern>

