<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ AVC_EDDiagnosisEntries.sch ]O@%#o>>=-
    
    Teste les entrées de la section "PCC ED Diagnosis Section" dans le cadre du volet AVC
    
    Historique :
    04/11/2013 : CRI : Création AVC_EDDiagnosisEntries20120306.sch

    
-->

<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="AVC_EDDiagnosisEntries-errors">

    
    <!-- ****** Contexte = section Endocrinien et Métabolique ****** -->
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.1.13.2.9"]'>
    
        <assert test='.//cda:entry/cda:observation/cda:value/@code="F-A5400"'>
            [AVC_EDDiagnosisEntries] La latéralité manuelle du patient doit être indiquée
        </assert>
        <assert test='.//cda:entry/cda:observation/cda:value/@code="DA-21100"'>
            [AVC_EDDiagnosisEntries] L'absence ou la présence de déficits moteurs 
            doit obligatoirement être indiquée
        </assert>

        
        <assert test='.//cda:entry/cda:observation/cda:value/@code="F-A2200"'>
            [AVC_EDDiagnosisEntries] L'absence ou la présence de troubles sensitif s
            doit obligatoirement être indiquée
        </assert>
        <assert test='.//cda:entry/cda:observation/cda:value/@code="F-A4580"'>
            [AVC_EDDiagnosisEntries] L'absence ou la présence d'ataxie
            doit obligatoirement être indiquée
        </assert>
        <assert test='.//cda:entry/cda:observation/cda:value/@code="DA-70080"'>
            [AVC_EDDiagnosisEntries] L'absence ou la présence de troubles visuels
            doit obligatoirement être indiquée
        </assert>

    </rule>
</pattern>


