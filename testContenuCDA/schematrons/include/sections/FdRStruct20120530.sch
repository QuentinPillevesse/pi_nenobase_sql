<?xml version="1.0" encoding="UTF-8"?>

<!--  ASIP Santé  Section: 1.2.250.1.213.1.1.2.31 -->

<!--                  -=<<o#%@O[ FdRStruct.sch ]O@%#o>>=-
    
    Teste la conformité de la section "Facteurs de Risques (1.2.250.1.213.1.1.2.31)
    aux spécifications du volet VSM du CI-SIS de l'ASIP Santé pour la version structurée.    
   Pas d'entrée.

    
    Historique :
    30/05/12 : CRI : Création
    
-->

<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="FdRStruct-errors">
    <title>CI-SIS Section Facteurs de Risques</title>
    
    <rule context='*[cda:templateId/@root="1.2.250.1.213.1.1.2.31"]'>
        <!-- Verifier que le templateId est utilisé pour une section -->
        <assert test='../cda:section'> 
            Erreur de Conformité : Ce template ne peut être utilisé que pour une section.
        </assert>         
        <!-- Vérification du la présence des sous-sections -->
        <!-- Coded Social History -->
        <assert test='.//cda:templateId[@root = "1.3.6.1.4.1.19376.1.5.3.1.3.16.1"]'>
            Erreur [FdRStruct] : La section 'Facteurs de risque' 
            doit contenir une sous-section 'Coded Social History'.
        </assert>  
        <!-- Hazardous Working Conditions -->
        <assert test='.//cda:templateId[@root = "1.3.6.1.4.1.19376.1.5.3.1.1.5.3.1"]'>
            Erreur [FdRStruct] : La section 'Facteurs de risque' 
            doit contenir une sous-section 'Hazardous Working Conditions'.
        </assert> 
        <!-- Family Medical History -->
        <assert test='.//cda:templateId[@root = "1.3.6.1.4.1.19376.1.5.3.1.3.15"]'>
            Erreur [FdRStruct] : La section 'Facteurs de risque' 
            doit contenir une sous-section 'Coded Family Medical History'.
        </assert> 
    </rule>
    
</pattern>