<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ EDDiagnosisSection.sch ]O@%#o>>=-
    
    Teste les entrées de la section "PCC ED Diagnosis Section" dans le cadre du volet AVC
    
    Historique :
    04/11/2013 : CRI : Création AVC_EDDiagnosisSection20120306.sch

    
-->

<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="AVC_EDDiagnosisSection-errors">

    
    <!-- ****** Contexte = section Endocrinien et Métabolique ****** -->
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.1.13.2.9"]'>

        <assert test='.//cda:entry/cda:observation/cda:code[@code="11301-9"]'>
            [AVC_EDDiagnosisSection] L'attribut code de l'élément "code" 
            d'une observation de l'examen physique est fixé à la valeur "11301-9"
        </assert>
        <assert test='.//cda:entry/cda:observation/cda:code[@codeSystem="2.16.840.1.113883.6.1"]'>
            [AVC_EDDiagnosisSection] L'attribut codeSystem de l'élément "code" 
            d'une observation de l'examen physique est fixé à la valeur "2.16.840.1.113883.6.1"
        </assert>
        

    </rule>
</pattern>

