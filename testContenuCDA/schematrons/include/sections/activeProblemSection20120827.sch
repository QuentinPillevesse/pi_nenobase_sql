<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ activeProblemSection.sch ]O@%#o>>=-
    
    Teste la conformité de la section " Active Problems Section" (1.3.6.1.4.1.19376.1.5.3.1.3.6)
    aux spécifications d'IHE PCC v3.0
    
    Historique :
    25/07/11 : CRI : Création
    
-->


<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="activeProblemSection-errors">


    <title>IHE PCC v3.0 Active Problems Section</title>

    <rule context="*[cda:templateId/@root=&quot;1.3.6.1.4.1.19376.1.5.3.1.3.6&quot;]">
        <!-- Verifier que le templateId est utilisé correctement -->
        <assert test="../cda:section"> 
            [activeProblemSection] : 'Active Problems' ne peut être utilisé que comme section.</assert>
        
        <!-- Vérifier la présence des templateId parents. -->
        <assert test="cda:templateId[@root=&quot;2.16.840.1.113883.10.20.1.11&quot;]"> 
            [activeProblemSection] : Le templateId parent de la section 'Active Problems' (2.16.840.1.113883.10.20.1.11) doit être présent</assert>
        
        <!-- Vérifier le code de la section -->
        <assert test="cda:code[@code = &quot;11450-4&quot;]"> 
            [activeProblemSection] : Le code de la section 'Active Problems' doit être '11450-4'</assert>
        
        <assert test="cda:code[@codeSystem = &quot;2.16.840.1.113883.6.1&quot;]"> 
            [activeProblemSection] : L'élément 'codeSystem' de la section 
            'Active Problems' doit être codé dans la nomenclature LOINC (2.16.840.1.113883.6.1)</assert>
        <!--  
        <assert test="cda:title='Pathologie en cours'">
            Erreur [activeProblemSection] : L'élément 'title' de la section 
            'Active Problems' est fixé à 'Pathologie en cours'</assert>
        -->

        <!-- Vérifier que la section contient des éléments Problem Concern Entry -->
        <assert test=".//cda:text">
            [activeProblemSection] : Une section "Active Problems" doit contenir un élément text"</assert>
    </rule>
</pattern>
