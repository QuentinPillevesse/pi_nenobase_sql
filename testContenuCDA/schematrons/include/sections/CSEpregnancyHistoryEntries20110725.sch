<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ CSEpregnancyHistoryEntries.sch ]O@%#o>>=-
    
    Teste les entrées de la section PCC Pregnancy History (1.3.6.1.4.1.19376.1.5.3.1.1.5.3.4) utilisée dans le volet du CI-SIS 
    "Certificats de santé de l'enfant du 8ème jour"
    
    Historique :
    25/07/11 : CRI : Création
    
    retirés : 
    @code="D8-110F9" 
    @code="R-F70D9" 
    @code="F-04821" 
    
-->

<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="CSEpregnancyHistoryEntries-errors">
    <title>IHE PCC v3.0 Pregnancy History Section</title>
    
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.4.13.5"]'> 
 
        <assert test='cda:code[@code="xx-MCH027" or
            
            @code="11977-6" or
            @code="11640-0" or
            @code="11637-6" or
            @code="XX-MCH182" or
            @code="XX-MCH183" or
            @code="xx-MCH019" or
            @code="11878-6" or
            @code="11884-4" or 
            @code="P2-87524" or
            @code="D8-20432" or
            @code="XX-MCH029" or
            @code="xx-MCH035" or
            @code="XX-MCH185" or
            @code="D8-11120" or
            @code="DB-61400" or
            @code="D8-11000" or
            @code="D8-70110" or
            @code="D8-12000" or
            @code="XX-MCH187" or
            @code="F-87000" or
            @code="xx-MCH135"]'>
            
            Erreur de Conformité volet CS8: L'attribut 'code' doit être codé selon les valeurs prévues dans le volet. 
        </assert>

        <assert test='not(cda:code[@code="xx-MCH027"]) or cda:value[@xsi:type="CD"]'>
            Erreur de Conformité volet CS8: l'élément "date de déclaration de grossesse" 
            est une donnée codée (@xsi:type="CD").           
        </assert>
    
        <assert test='not(cda:code[@code="11977-6"]) or cda:value[@xsi:type="INT"]'>
            Erreur de Conformité volet CS8: l'élément "Gestité" s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test='not(cda:code[@code="11640-0"]) or cda:value[@xsi:type="INT"]'>
            Erreur de Conformité volet CS8: l'élément "Parité" s'exprime en entier (@xsi:type="INT").           
        </assert>
        <assert test='not(cda:code[@code="11637-6"]) or cda:value[@xsi:type="INT"]'>
            Erreur de Conformité volet CS8: l'élément "Nb d'enfants nés avant 37 semaines" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test='not(cda:code[@code="XX-MCH182"]) or cda:value[@xsi:type="INT"]'>
            Erreur de Conformité volet CS8: l'élément "Nb d'enfants pesant moins de 2500g" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test='not(cda:code[@code="XX-MCH183"]) or cda:value[@xsi:type="INT"]'>
            Erreur de Conformité volet CS8: l'élément "Nb d'enfants morts-nés" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test='not(cda:code[@code="xx-MCH019"]) or cda:value[@xsi:type="INT"]'>
            Erreur de Conformité volet CS8: l'élément "Nb d'enfants morts avant 28 jours" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test='not(cda:code[@code="11878-6"]) or cda:value[@xsi:type="INT"]'>
            Erreur de Conformité volet CS8: l'élément "Nb de Foetus" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test='not(cda:code[@code="11884-4"]) or cda:value[@unit="wk"]'>
            Erreur de Conformité volet CS8: l'élément "L'âge gestationnel" 
            s'exprime en semaines (value[@unit="wk"]).
        </assert>
        <assert test='not(cda:code[@code="D8-20432"]) or cda:value[@xsi:type="BL"]'>
            Erreur de Conformité volet CS8: l'élément "Antécédents de Césarienne" 
            est un booléen (@xsi:type="BL").
        </assert>
        <assert test='not(cda:code[@code="P2-87524"]) or cda:value[@xsi:type="BL"]'>
            Erreur de Conformité volet CS8: l'élément "Transfert in utero" 
            est un booléen (@xsi:type="BL").
        </assert>
        <assert test='not(cda:code[@code="XX-MCH029"]) or cda:value[@xsi:type="INT"]'>
            Erreur de Conformité volet CS8: l'élément "Rang de naissance" 
            s'exprime en entier sans unité (@xsi:type="INT").           
        </assert>
        <assert test='not(cda:code[@code="xx-MCH035"]) or cda:value[@xsi:type="BL"]'>
            Erreur de Conformité volet CS8: l'élément "Préparation à la naissance" 
            est un booléen (@xsi:type="BL").
        </assert>
    </rule>
</pattern>