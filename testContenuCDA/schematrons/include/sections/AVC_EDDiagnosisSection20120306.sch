<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ AVC_EDDiagnosisSection.sch ]O@%#o>>=-
    
    Teste les entrées de la section "PCC ED Diagnosis Section" dans le cadre du volet AVC
    
    Historique :
    04/11/2013 : CRI : Création AVC_EDDiagnosisSection20120306.sch

    
-->

<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="AVC_EDDiagnosisSection-errors">

    
    <!-- ****** Contexte = section Endocrinien et Métabolique ****** -->
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.1.13.2.9"]'>

        <assert test='.//cda:entry/cda:observation/cda:code[@code="11301-9"]'>
            [AVC_EDDiagnosisSection] L'attribut code de l'élément "code" 
            d'une observation de l'examen physique est fixé à la valeur "11301-9"
        </assert>
        <assert test='.//cda:entry/cda:observation/cda:code[@codeSystem="2.16.840.1.113883.6.1"]'>
            [AVC_EDDiagnosisSection] L'attribut codeSystem de l'élément "code" 
            d'une observation de l'examen physique est fixé à la valeur "2.16.840.1.113883.6.1"
        </assert>
        <assert test='.//cda:entry/cda:observation[@negationInd]/cda:code/@code="F-01250"'>
            [AVC_EDDiagnosisSection] L'absence ou la présence de symptômes neurologiques 
            doit obligatoirement être mentionnée à partir du booléen observation/@negationInd
        </assert>
        
        <assert test='.//cda:entry/cda:observation/cda:value/@code="F-A5400"'>
            [AVC_EDDiagnosisSection] La latéralité manuelle du patient doit être indiquée
        </assert>
        <assert test='.//cda:entry/cda:observation/cda:value/@code="DA-21100"'>
            [AVC_EDDiagnosisSection] L'absence ou la présence de déficits moteurs 
            doit obligatoirement être indiquée
        </assert>
        <assert test='.//cda:entry/cda:observation/cda:value/@code="F-A2200"'>
            [AVC_EDDiagnosisSection] L'absence ou la présence de troubles sensitif s
            doit obligatoirement être indiquée
        </assert>
        <assert test='.//cda:entry/cda:observation/cda:value/@code="F-A4580"'>
            [AVC_EDDiagnosisSection] L'absence ou la présence d'ataxie
            doit obligatoirement être indiquée
        </assert>
        <assert test='.//cda:entry/cda:observation/cda:value/@code="DA-70080"'>
            [AVC_EDDiagnosisSection] L'absence ou la présence de troubles visuels
            doit obligatoirement être indiquée
        </assert>

    </rule>
</pattern>

