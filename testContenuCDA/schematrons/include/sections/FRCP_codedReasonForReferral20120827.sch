<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ FRCP_codedReasonForReferral.sch ]O@%#o>>=-
    
    Teste la conformité du contenu de la section "Raison de la recommandation" (1.3.6.1.4.1.19376.1.5.3.1.3.2)
    aux spécifications d'IHE PCC v3.0
    
    Historique :
    25/07/11 : CRI : Création
    
-->


<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="FRCP_codedReasonForReferral-errors">
    <title>IHE PCC v3.0 Procedures Section</title>

    <title>IHE PCC v3.0 FRCP_codedReasonForReferral Section</title>
        
        
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.5.3.1.3.2"]'> 
            <!-- Verifier que le templateId est utilisé correctement --> 
        <assert test="./cda:entry/cda:observation/cda:code[@code='RCP_026']">
            Erreur [FRCP_codedReasonfForReferral]: L'avis diagnostique (RCP_026) est une raison qui doit être examiné individuellement.
        </assert>


        <!-- Verifier que le templateId est utilisé correctement --> 
        <assert test="./cda:entry/cda:observation/cda:code[@code='RCP_027']">
            Erreur [FRCP_codedReasonfForReferral]: La proposition de traitement -- dont ajustement et surveillance (RCP_027) est une raison qui doit être examiné individuellement. 
        </assert>


        <!-- Verifier que le templateId est utilisé correctement --> 
        <assert test="./cda:entry/cda:observation/cda:code[@code='RCP_030']">
            Erreur [FRCP_codedReasonfForReferral]: La demande de recours (RCP_030) est une raison qui doit être examiné individuellement.
        </assert>
    </rule>
</pattern>