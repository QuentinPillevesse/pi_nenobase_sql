<?xml version="1.0" encoding="UTF-8"?>

<!--                  -=<<o#%@O[ childFunctionalStatusPsychoMot.sch ]O@%#o>>=-
    
    Teste la conformité de la section "Psychomotor Development" (1.3.6.1.4.1.19376.1.7.3.1.1.13.4)
    aux spécifications d'IHE PCC v3.0
    
    Note: La section est codée avec le code provisoire xx-MCH-PsychoMDev, provenant de la nomenclature
    TA_CS, provisoirement avant son intégration dans la nomenclature LOINC.
    Lorsque celle-ci sera effective, la modification des schematrons correspondants sera 
    effectuée.
    la nomenclature de
    
    Historique :
    24/06/11 : CRI : Création
    
-->


<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="childFunctionalStatusPsychoMot-errors">
    <title>IHE PCC v3.0 Psychomotor Development</title>
    <rule context='*[cda:templateId/@root="1.3.6.1.4.1.19376.1.7.3.1.1.13.4"]'> 
        <!-- Verifier que le templateId est utilisé à bon escient -->        
        <assert test='../cda:section'> 
            Erreur de Conformité au volet PCC: 'Psychomotor Development' ne peut être utilisé que comme section.
        </assert>

        <!-- Vérifier le code de la section --> 
        <assert test='cda:code[@code = "xx-MCH-PsychoMDev"]'> 
            Erreur de Conformité au volet PCC: Le code de la section 'Psychomotor Development' doit être 'xx-MCH-PsychoMDev'
        </assert> 
        <assert test='cda:code[@codeSystem = "1.2.250.1.213.1.1.4.14"]'> 
            Erreur de Conformité au volet PCC: L'élément 'codeSystem' de la section 'Psychomotor Deveopment' doit être codé dans la nomenclature TA_CS 
            (1.2.250.1.213.1.1.4.14). 
        </assert> 


    </rule>
</pattern>
